from java2ta.ir.models import *
from java2ta.translator.shortcuts import *
from java2ta.abstraction.shortcuts import *
from java2ta.abstraction.models import *


DEFAULT_PROJECT_PATH = '../projects/'
#DEFAULT_SCRIPT_PATH = '../scripts/'
DEFAULT_FSA_PATH = '../fsas/'
DEFAULT_API_URL = 'localhost:9000'

DEFAULT_DELAY = 3
MAX_PROJ_OPEN_ATTEMPTS = 3



# dizionario che associa ad ogni tipo di dato (stringa) il rispettivo dominio
DATATYPES = {
#    'type_name' : (datatype, default_domain)
    'Integer'   : (Integer(),   INTEGERS),
    'Boolean'   : (Boolean(),   BOOLEANS),
    'String'    : (AbsString(), STRINGS),
    'Object'    : (Object(),    OBJECTS),
    'Iterator'  : (Iterator(),  ITERATORS),
}

# dizionario che associa ad ogni operatore (stringa) la rispettiva classe
OPERATORS = {
    '<'    : LT,
    '<=': LTE, 
    '>'    : GT,
    '>=': GTE,
    '='    : Eq,
    '!=': NotEq,
}

