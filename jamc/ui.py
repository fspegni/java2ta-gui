from jamc import config #*
from jamc.errors import CommandError
import sys
import traceback
import shlex


# contiene la logica che permette l'elaborazione e l'esecuzione dei comandi
class UI:

    """
    __standard_commands:

        dizionario dei comandi di base per il funzionamento del tool (definiti nel costruttore),
        questi comandi hanno effetto solo sugli application_commands

    __application_commands:

        dizionario dei comandi specifici dell'applicazione, ognuno di essi ha una classe apposita
        e vengono definiti di volta in volta dal programmatore a seconda delle esigenze,
        la loro inizializzazione viene fatta richiamando il metodo set_application_commands(commands)

    __undo_stack:

        pila che contiene i comandi eseguiti in precedenza con i rispettivi stati che possono 
        essere recuperati tramite il comando undo, un elemento della pila e' una tupla contenente:
        - una stringa contenente l'i-esima LINEA DI COMANDO (comando e parametri)
        - un dizionario che descrive l'i-esimo STATO (precedente all'esecuzione del comando)
            la cui struttura non e' predefinita e viene gestita dalle classi associate ai comandi 
            tramite i metodi opportuni

    __redo_stack = None
        pila che contiene i comandi eseguiti in precedenza ed annullati tramite il comando undo,
        SENZA che siano stati eseguiti altri comandi (in tal caso la pila viene svuotata),
        redo_stack[i] e' una stringa contenente l'i-esima LINEA DI COMANDO, che al comando redo 
        viene semplicemente rieseguito
    """

    # inizializza gli attributi, qui vengono inoltre definiti i comandi standard
    def __init__(self, trace=False):
        self.__application_commands = {}
        self.__standard_commands = {'help'      : self.cmd_help,
                                    'undo'      : self.cmd_undo,
                                    'redo'      : self.cmd_redo,
                                    'history'   : self.cmd_history,
                                    'load'      : self.cmd_load,
                                    'store'     : self.cmd_store,
                                    'quit'      : self.cmd_quit,
                                    'pwd'       : self.cmd_pwd,
                                    'cd'        : self.cmd_cd,
                                    }
        self.__undo_stack = []
        self.__redo_stack = []
        self.trace = trace

    # stampa la lista dei comandi con la loro descrizione
    def cmd_help(self, params):

        pad_size = max(map(len, self.__application_commands.keys() + self.__standard_commands.keys()))

        print "List of available commands:\n"
        for command_name, command in sorted(self.__application_commands.items()):
            padded_name = command_name.ljust(pad_size+1, " ")
            print " %s: %s" % (padded_name, command.description)


    # estrae dalla pila undo_stack l'ultimo comando eseguito ed esegue il metodo undo() della classe del COMANDO in questione,
    # tale metodo prende come parametro un dizionario di informazioni sullo STATO precedente che verra' ripristinato,
    # infine inserisce il COMANDO nella pila redo_stack e stampa il comando annullato
    # senza verificare la corretta esecuzione di esso (l'undo non puo' fallire)
    def cmd_undo(self, params):
        try:
            (command_line, previous_state) = self.__undo_stack.pop()
            (str_command, params) = self.__split_command_params(command_line)
            command = self.__application_commands[str_command]
            command.undo(previous_state)
            self.__redo_stack.append(command_line)
#            print('Command undone: ' + command_line)
        except IndexError:
            raise CommandError('Nothing to undo')


    def cmd_redo(self, params):
        """
        Pop the most recent command from the stack un-done commands
        """
        try:
            command_line = self.__redo_stack.pop()
            if len(command_line) > 0 and command_line[0] != "#":
                # not empty nor comment
                #print "Redo: %s" % command_line
                self.command(command_line, True)
        except IndexError:
            raise CommandError('Nothing to redo')


    def cmd_history(self, params):
        """
        Print the history of commands executed so far, and contained in
        the command stack.
        """
        if len(self.__undo_stack) > 0:
            for command_line, previous_state in self.__undo_stack:
                print command_line


    def cmd_load(self, params):
        """
        Load the file with the passed name and execute the commands
        contained there.
        """
        file_name = params[0]

        if file_name == "-h":
            # print help message
            print "usage: load [-h] path"
            print
            print "Execute commands contained in a script file"
            print
            print "positional arguments:"
            print "  path       filepath of the script"
            print
            print "-h : print this help and exits"
            return

        try:
            with open(file_name, 'r') as cmd_file:
                for line in cmd_file:
                    if len(line) > 0 and line[0] != "#":
                        # not empty line nor comment
                        self.command(line.strip(), echo=True)

#            print('Commands in the file "' + file_name + '" successfully executed')
        except IOError, e:
#            print('File "' + file_name + '" not found')
            raise CommandError("Error reading file: %s. Details: %s" % (file_name, e))

    # salva nel file con il nome passato come parametro la cronologia dei comandi contenuti nella pila undo_stack, riga per riga
    def cmd_store(self, params):
        file_name = params[0]

        try:
            file = open(file_name, 'w')
            for command_line, previous_state in self.__undo_stack:
                file.write(command_line + '\n')
            file.close()
            print('Commands in history successfully saved in the file "' + file_name + '"')
        except IOError:
            print('File "' + file_name + '" not found')


    # chiude il programma
    def cmd_quit(self, *args):
        print('Bye')
        sys.exit()

    # stampa la directory corrente
    def cmd_pwd(self, params):
        print os.getcwd()

    def cmd_cd(self, params):
        try:
            os.chdir(params[0])
        except OSError, e:
            print "Error changing directory: %s" % e


    # divide la linea di comando (stringa) in una stringa contenente il comando e una lista contenente i parametri
    def __split_command_params(self, command_line):
        command_line = shlex.split(command_line)
        command = None
        params = None

        try:
            command = command_line[0]
            params = command_line[1:]
        except IndexError:                            # o la riga di comando e' vuota (si puo' verificare solo in caso di input dell'utente, in tal caso l'errore verra' gestito dal metodo command(), o non ci sono parametri (cosa possibile, quindi ignoriamo l'errore)
            pass

        return (command, params)


    # inizializza i comandi dell'applicazione, il dizionario che li contiene viene preso come parametro
    # perche' questo metodo viene richiamato da init.py prima di avviare il programma vero e proprio,
    # in questo modo il programmatore definisce i comandi nel file init.py e non c'e' bisogno di modificare questa classe
    def set_application_commands(self, commands):
        self.__application_commands = commands

    def add_command(self, name, command_class):
        self.__application_commands[name] = command_class(trace=self.trace)

    def get_application_commands(self):
        return self.__application_commands

    def get_command_names(self):
        return sorted(self.__application_commands.keys() + self.__standard_commands.keys())

##    # richiamato da assert_error e assert_success, questo metodo contiene la logica per effettuare le asserzioni,
##    # prende come input una serie di argomenti (stringhe) e verifica che siano contenute nell'ultimo output visualizzato,
##    # memorizzato nella stringa DEBUG_LOG
##    def __assert_log(self, type, missing, *args):
##        debug_log = GET_DEBUG_LOG()
##        try:
##            assert type in debug_log
##            for log in args:
##                if missing:
##                    assert log not in debug_log
##                else:
##                    assert log in debug_log
##        except AssertionError as e:
##            print('DEBUG LOG:')
##            print(debug_log)
##            raise AssertionError
##        print('.')
##
##
##    # verifica che l'operazione sia fallita e che eventualmente il tipo di errore sia quello voluto
##    def assert_error(self, *args):
##        self.__assert_log('Error', False, *args)
##
##
##    # verifica che l'operazione sia andata a buon fine e che eventualmente l'output sia quello voluto
##    def assert_success(self, *args):
##        self.__assert_log('Success', False, *args)
##
##
##    # verifica che l'operazione sia andata a buon fine e che non 
##    # vengano visualizzate determinate informazioni nell'output
##    def assert_missing_success(self, *args):
##        self.__assert_log('Success', True, *args)
##

    # prende come input una riga inserita nel prompt (un comando con i 
    # suoi parametri), quindi effettua il parsing del comando e lo esegue
    def command(self, input, redoing = False, echo=False):

        if echo:
            print "$ %s" % input

        (str_command, params) = self.__split_command_params(input)

        if str_command is None: # input vuoto
            return

        if str_command in self.__standard_commands: # standard command
#            try:
            command = self.__standard_commands[str_command]
            command(params)
#            except IndexError:
#                # numero di parametri passati insufficiente, ad 
#                # esempio load e store senza il parametro file
#                print('Command not executed, insufficient parameters')
#            except CommandError, e:
#                if self.trace:
#                    traceback.print_exc()
#                print "Error! %s" % e
        elif str_command in self.__application_commands:    
            # application command
#            try:
            command = self.__application_commands[str_command]
            previous_state = command.run(str_command, params).copy()
            # esegue il comando
            command_state = (input, previous_state)
            self.__undo_stack.append(command_state)
            if not redoing:
                self.__redo_stack[:] = []                # esecuzione (non redo) di un nuovo comando, quindi viene svuotata la __redo_stack
#            except SystemExit:                                # parametri sbagliati
#                print('Command not executed')
#            except IOError as e:                                    
#                # se l'esecuzione di questo comando e' partita dal 
#                # metodo cmd_load genera un IOError, l'errore deve 
#                # essere gestito qui e non da cmd_load, che deve 
#                # gestire solo gli errori relativi all'apertura del file
#                if self.trace:
#                    traceback.print_exc()
#                print 'Error! %s' % e
#            except CommandError as e:                        # errore durante l'esecuzione del comando
#                if self.trace:
#                    traceback.print_exc()
#                print 'Error! %s' % e
        else:                                                # comando sbagliato
            print('Command "' + str_command + '" not found')
