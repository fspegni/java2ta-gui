from jamc import create_ui
from jamc.models import Model
from jamc.ui import UI
from jamc.errors import CommandError

from java2ta.abstraction.models import StateSpace, AbstractAttribute

def test_create_ui():
    
    ui = create_ui()
    assert isinstance(ui, UI), type(ui)

    commands = ui.get_application_commands()
    assert len(commands) > 0
    assert len(commands) == 35, len(commands)

def test_ss_commands():

    ui = create_ui()

    try:
        # casi limite
        ui.command('ss_show ss1')							# spazio degli stati inesistente
        assert False, "Expected CommandError for state space not being created"
    except CommandError, e:
        assert "doesn't exist" in e.message
    except Exception, e:
        assert False, "Error not expected: %s" % e

    try:
        # setup per caso base
        ui.command('ss_create ss1')
        ui.command('ss_attr_create ss1 attr1 var1:Integer,var2:Integer "(< {var1} {var2}),(= {var1} {var2}),(> {var1} {var2})"')
        ui.command('ss_show ss1')
        assert True
    except Exception, e:
        assert False, "Error not expected: %s" % (e,)

    model = Model.Instance()
    all_ss = model.get_statespaces()
    assert len(all_ss) == 1
        
    ss_attr1 = model.get_statespace_attributes("ss1")
    assert isinstance(ss_attr1, dict)
    assert len(ss_attr1) == 1, len(ss1) # length of statespace is given by its attributes

    ss1 = model.get_statespace("ss1")
    assert isinstance(ss1, StateSpace)  
    ss_attr2 = ss1.get_attributes()
    assert isinstance(ss_attr2, list)
    assert len(ss_attr2) == len(ss_attr1)
    assert len(ss1.attributes) == len(ss_attr1)

    try:
        attr1 = ss1.get_attribute("var1__var2")  # NB at the moment the name of the attribute is not the one passed on the tool, but it's computed automatically based on the variables appearing in the predicates
        assert True
    except ValueError:
        assert False, "Expected attribute with name 'attr1'. Not found. All attributes: %s" % (ss1.attributes)
    assert isinstance(attr1, AbstractAttribute)

def test_ss_create():

    ui = create_ui()

    model = Model.Instance()

    ui.command('ss_create ss1')

    ss1 = model.get_statespace_attributes("ss1")
    assert isinstance(ss1,dict)
    assert len(ss1) == 0

    ss1 = model.get_statespace("ss1")
    assert isinstance(ss1, StateSpace)  
    assert len(ss1.attributes) == 0

    ui.command('ss_list')
    # TODO how to check this?

    try:
        ui.command('ss_create ss1')
        assert True
    except e:
        assert False, "Error not expected even though a state space with the same name exists"


    ui.command('ss_create ss2')
    ui.command('ss_attr_create ss1 attr1 var1:Integer,var2:Integer "(< {var1} {var2}),(= {var1} {var2}),(> {var1} {var2})"')
    
    ss1 = model.get_statespace_attributes("ss1")
    assert isinstance(ss1,dict)
    assert len(ss1) == 1

    ss1 = model.get_statespace("ss1")
    assert isinstance(ss1, StateSpace)  
    assert len(ss1.attributes) == 1

    ui.command("ss_list -v")
    # TODO how to test this?

def test_ss_delete():

    ui = create_ui()

    ui.command('ss_create ss1')
    ui.command('ss_create ss2')

    try:
        # casi limite
        ui.command('ss_del wrongstatespace')							# spazio degli stati inesistente
        assert False, "Expected CommandError for state space not being created"
    except CommandError, e:
        assert "doesn't exist" in e.message
    except Exception, e:
        assert False, "Error not expected: %s" % e

    try:
        ui.command('ss_del ss1')
        ui.command('ss_del ss2')
        assert True
    except Exception, e:
        assert False, "Error not expected: %s" % e

    try:
        ui.command('ss_del ss1')
        assert False, "Error expected for state space not existing anymore"
    except CommandError, e:
        assert "doesn't exist" in e.message

    try:
        ui.command('ss_del ss2')
        assert False, "Error expected for state space not existing anymore"
    except CommandError, e:
        assert "doesn't exist" in e.message



def test_ss_rename():

    ui = create_ui()

    ui.command('ss_create ss1')
    ui.command('ss_create ss2')

    try:
        # casi limite
        ui.command('ss_ren ss3 ss4') # error: ss3 does not exist
        assert False, "Expected CommandError for state space with given name not existing"
    except CommandError, e:
        assert "doesn't exist" in e.message
    except Exception, e:
        assert False, "Error not expected: %s" % e

    try:
        ui.command('ss_ren ss1 ss1') # error: same name
        assert False, "Error expected for renaming the state space with its own name"
    except CommandError, e:
        assert "same name" in e.message, e

    ui.command('ss_ren ss1 ss3')
    ui.command('ss_ren ss3 ss4') # now this command does not raise an error
    ui.command('ss_del ss4') 
    ui.command('ss_del ss2')
    
    try:
        ui.command('ss_del ss1')
        assert False, "Error expected for deleting a state space that should not exist anymore"
    except CommandError, e:
        assert "doesn't exist" in e.message

def test_proj_open():
    ui = create_ui()

    try:
        ui.command('proj_open projects/wrongProject')
        assert False, "Error expected for project not existing"
    except CommandError, e:
        assert "cannot open the project" in e.message, e

    
    # this works only if run just outside the jamc directory
    # TODO find a way to run the test from every path in the system
    ui.command('proj_open projects/test1')
    ui.command('proj_classes')
    # TODO how to test this?

    ui.command('proj_close')

    try:
        ui.command('proj_close') # error: it's the second time we are closing the project
        assert False, "Error expected because we are closing twice the same project"
    except CommandError, e:
        assert "Must open a project first" in e.message


def test_formulas():

    ui = create_ui()

    ui.command('proj_open projects/testProject')
    ui.command('ss_create ss1')
    ui.command('ss_attr_create ss1 attr1 a:Integer,b:Integer "(= {a} {b}),(!= {a} {b})"')
    ui.command('ss_attr_create ss1 i i:Integer "(< {i} 0),(= {i} 0),(> {i} 0)"')
    ui.command('formula_add f1 "(and [(< {i} 0)] [(= {a} {b})])" ss1')
    ui.command('formula_print f1')

