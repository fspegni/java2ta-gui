from jamc.commands import Command
from jamc.errors import CommandError
import logging
import sys
import os

LOG_CONFIG = {
	"version": 1,
	"disable_existing_loggers": 0,
	"root": {
		"level": "DEBUG",
		"handlers": [
			"console",
#			"file",
		]
	},
	"loggers": {
		"requests.packages.urllib3.connectionpool": { "level":"WARNING"},
        "java2ta.translator.shortcuts": { "level":"DEBUG" },
        "java2ta.ir.client": { "level":"WARNING" },
        "smt": {"level":"DEBUG"},
        "jamc.commands.method": { "level":"DEBUG" },
	},
	"formatters": {
		"precise": {
			"format": "%(levelname)s:%(asctime)s:%(name)s:%(message)s"
		},
		"brief": {
			"format": "%(levelname)s:%(message)s"
		},
        "basic": {
            "format": "%(levelname)s:%(message)s"
        }
	},
	"handlers": {
		"console": {
			"formatter": "brief",
			"class": "logging.StreamHandler",
			"stream": "ext://sys.stdout",
			"level": "WARNING"
		},
#		"file": {
#			"formatter": "precise",
#			"level": "DEBUG",
#            "class": "logging.FileHandler",
#			"filename": "%s.log" % (os.path.basename(sys.argv[0]),),
#		}
	}
}


class CommandLog(Command):

    LOG_LEVEL_DECODE = {
        "DEBUG": logging.DEBUG,
        "INFO": logging.INFO,
        "WARNING": logging.WARNING,
        "ERROR": logging.ERROR,
    }

    description = 'Enable/disable/configure the logging system'

    def __init__(self, *args, **kwargs):
        super(CommandLog, self).__init__(*args, **kwargs)

        self.log_config = LOG_CONFIG
#        self.log_status = "on"
        self.set_log_level("DEBUG")
#        self.do_on()
        self.do_off()

    def get_log_file(self):
#        return self.log_config["handlers"]["file"]["filename"]
        log_file = self.log_config.get("handlers", {}).get("file", {}).get("filename", None)

        return log_file

    def set_log_file(self, filename):
#        self.log_config["handlers"]["file"]["filename"] = filename
        self.log_config["handlers"]["file"] = {
			"formatter": "precise",
#			"level": "DEBUG",
            "level": self.LOG_LEVEL_DECODE.get(self.log_level, logging.NOTSET),
            "class": "logging.FileHandler",
			"filename": filename,
        }
        logging.config.dictConfig(self.log_config)

    def set_log_level(self, value):
        self.log_level = value
        if "file" in self.log_config["handlers"]:
            self.log_config["handlers"]["file"]["level"] = self.LOG_LEVEL_DECODE.get(value, logging.NOTSET)
        logging.config.dictConfig(self.log_config)


    def _parse(self, parser):

        parser.add_argument('action', type=str, help='on,off,status,sel_level,set_file')
        parser.add_argument('value', type=str, nargs='?', help='an additional value for set_* commands')

    def _set_previous_state(self, previous_state, args):
        previous_state["log_status"] = self.log_status
        previous_state["log_config"] = self.log_config
        previous_state["log_level"] = self.log_level

    def _do(self, args):

        if args.action == "on":
            self.do_on()
        elif args.action == "off":
            self.do_off()
        elif args.action == "status":
            self.do_status()
        elif args.action == "set_level":
            self.set_log_level(args.value)
        elif args.action == "set_file":
            self.set_log_file(args.value)
        else:
            raise CommandError("Action '%s' is not recognized. Use one of the following: on,off,status")


    def do_on(self):
        log_file = self.log_config["handlers"].get("file", None)
        if log_file is not None and "file" not in self.log_config["root"]["handlers"]:
            self.log_config["root"]["handlers"].append("file")

        logging.config.dictConfig(self.log_config)
        logging.disable(logging.NOTSET)


        self.log_status = "on"

    def do_off(self):
        logging.disable(logging.ERROR)
        self.log_status = "off"

    def do_status(self):
        print "Log status: %s. Log file: %s. Log level: %s." % (self.log_status, self.get_log_file(), self.log_level)

    def undo(self, state):
        self.log_status = state["log_status"]
        self.log_config = state["log_config"]
        self.log_level = state["log_level"]        

        if self.log_status == "on":
            self.do_on()
        elif self.log_status == "off":
            self.do_off()
        else:
            raise CommandError("Log status '%s' not recognized")

