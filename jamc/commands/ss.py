from jamc.commands import Command
from jamc.models import Model
from java2ta.translator.models import KnowledgeBase
from java2ta.abstraction.models import AttributeStatus

import copy

class CommandSsCreate(Command):

    _previous_state = {}

    _args = None

    description = 'Creates an empty state space.'

    def _parse(self, parser):
        parser.add_argument('name', type=str, help='name of the state space to create')

    def _set_previous_state(self, previous_state, args):
        previous_state['name'] = args.name

    def _do(self, args):
        Model.Instance().add_statespace(args.name)

    def undo(self, state):
        Model.Instance().delete_statespace(state['name'])


class CommandSsDel(Command):

    _previous_state = {}

    _args = None

    description = 'Deletes a state space.'

    def _parse(self, parser):
        parser.add_argument('name', type=str, help='name of the state space to delete')

	def _set_previous_state(self, previous_state, args):
		previous_state['statespace_name'] = args.name
		previous_state['statespace'] = copy.copy(Model.Instance().get_statespace_attributes(args.name))

    def _do(self, args):
        Model.Instance().delete_statespace(args.name)
        return 'The state space "' + args.name + '" has been successfully deleted'

    def undo(self, state):
        Model.Instance().undo_delete_statespace(state['statespace_name'], state['statespace'])


class CommandSsList(Command):

    _args = None

    description = 'Shows all the created state spaces.'

    def _parse(self, parser):
        parser.add_argument('-v', '--verbose', action='store_true', help='shows more detailed information')

    def _set_previous_state(self, previous_state, args):
        return

    def _do(self, args):
        statespaces = Model.Instance().get_statespaces()
        output = '************* State spaces list *************\n'
        if args.verbose:
            output += '\n-----------------------'
        for statespace_name, statespace in statespaces.items():
            if args.verbose:
                output += '\nState space name: ' + statespace_name
                output += '\n' + Model.Instance().statespace_to_string(statespace)
                output += '\n\n-----------------------'
            else:
                output += '\n- ' + statespace_name
        return output

    def undo(self, state):
        return

class CommandSsRen(Command):

    _previous_state = {}

    _args = None

    description = 'Renames the current state space.'

    def _parse(self, parser):
        parser.add_argument('old_name', type=str, help='current name of the state space to rename')
        parser.add_argument('new_name', type=str, help='new name of the state space')

    def _set_previous_state(self, previous_state, args):
        previous_state['old_name'] = args.old_name
        previous_state['new_name'] = args.new_name

    def _do(self, args):
        Model.Instance().rename_statespace(args.old_name, args.new_name)
        return 'The state space "' + args.old_name + '" has been successfully renamed to "' + args.new_name + '"'

    def undo(self, state):
        Model.Instance().rename_statespace(state['new_name'], state['old_name'])

class CommandSsShow(Command):

    _previous_state = {}

    _args = None

    description = 'Shows the state space with the given name.'

    def _parse(self, parser):
        parser.add_argument('name', type=str, help='name of the state space to show')

    def _set_previous_state(self, previous_state, args):
        return

    def _do(self, args):
        statespace = Model.Instance().get_statespace_attributes(args.name)
        return '\n\nState space name: ' + args.name + '\n\n' + Model.Instance().statespace_to_string(statespace)

    def undo(self, state):
        return


class CommandSsAttrCreate(Command):

    _previous_state = {}

    _args = None

    description = 'Creates a new attribute (with variables and associated predicates) and associates it to a state space.'

    def _parse(self, parser):
        attribute_statuses = [ status.name.lower() for status in AttributeStatus ]

        parser.add_argument('ss_name',        type=str, help='name of the state space')
        parser.add_argument('attr_name',    type=str, help='name of the attribute to create (if this name already exists in the current state space, the attribute will be replaced)')
        parser.add_argument('var_list',        type=str, help='list of variables with their domains, with the format: "variable1:domain1,variable2:domain2,[...]variableN:domainN" (e.g. "i:INTEGERS,tag:STRINGS");')
        parser.add_argument('pred_list',    type=str,  help='list of predicates (boolean expressions on the given variables), with the format: "[operand1.1][operator1][operand1.2],[operand2.1][operator2][operand2.2],[...]" (e.g. "i>0,tag1=tag2")')
        parser.add_argument('--status', type=str, default=attribute_statuses[0], help='the allowed type of this attribute (availables: %s; default: %s)' % (", ".join(attribute_statuses), attribute_statuses[0]))

    def _set_previous_state(self, previous_state, args):
        previous_state['ss_name'] = args.ss_name
        previous_state['attr_name'] = args.attr_name

    def _do(self, args):
        status = AttributeStatus[args.status.upper()]
        Model.Instance().add_attribute(args.ss_name, args.attr_name, args.var_list, args.pred_list, status)
        return 'The attribute "' + args.attr_name + '" has been successfully associated to the state space "' + args.ss_name + '"'

    def undo(self, state):
        Model.Instance().delete_attribute(state['ss_name'], state['attr_name'])

class CommandSsAttrDel(Command):

    _previous_state = {}

    _args = None

    description = 'Deletes an attribute from a state space.'

    def _parse(self, parser):
        parser.add_argument('ss_name',        type=str, help='name of the state space to which the attribute to delete is associated')
        parser.add_argument('attr_name',    type=str, help='name of the attribute to delete')

    def _set_previous_state(self, previous_state, args):
        previous_state['ss_name'] = args.ss_name
        previous_state['attr_name'] = args.attr_name
        previous_state['attribute'] = copy.copy(Model.Instance().get_attribute_by_name(args.ss_name, args.attr_name))

    def _do(self, args):
        Model.Instance().delete_attribute(args.ss_name, args.attr_name)
        return 'The attribute "' + args.attr_name + '" has been successfully deleted from the state space "' + args.ss_name + '"'

    def undo(self, state):
        Model.Instance().undo_delete_attribute(state['ss_name'], state['attr_name'], state['attribute'])

class CommandSsAttrRen(Command):

    _previous_state = {}

    _args = None

    description = 'Renames an attribute associated to a state space.'

    def _parse(self, parser):
        parser.add_argument('ss_name',    type=str, help='name of the state space to which the attribute to rename is associated')
        parser.add_argument('old_name',    type=str, help='current name of the attribute to rename')
        parser.add_argument('new_name',    type=str, help='new name of the attribute')

    def _set_previous_state(self, previous_state, args):
        previous_state['ss_name'] = args.ss_name
        previous_state['old_name'] = args.old_name
        previous_state['new_name'] = args.new_name

    def _do(self, args):
        Model.Instance().rename_attribute(args.ss_name, args.old_name, args.new_name)
        return 'The attribute "' + args.old_name + '" associated to the state space "' + args.ss_name + '" has been successfully renamed to "' + args.new_name + '"'

    def undo(self, state):
        Model.Instance().rename_attribute(state['ss_name'], state['new_name'], state['old_name'])


class CommandSsAddTimestamp(Command):

    def _parse(self, parser):
        parser.add_argument('ss_name',    type=str, help='name of the state space to which the attribute to rename is associated')
        parser.add_argument('method_name', type=str, help='method name containing the timestamp variable')
        parser.add_argument('var_name',    type=str, help='name of timestamp variable')
        parser.add_argument('nature', type=str, choices=["absolute","relative"], default="absolute", help="the nature of the timestamp variable (absolute or relative)")

    def _do(self, args):
        method = Model.Instance().get_method(args.method_name)
        print "found method: %s" % method.name
        class_fqn = method.parent.fqname

        is_relative = (args.nature == "relative")

        KnowledgeBase.add_timestamp(class_fqn, method.name, args.var_name, is_relative)

