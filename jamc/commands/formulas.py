from jamc.commands import Command
from jamc.models import Model
from jamc.errors import CommandError
from java2ta.ta.models import TA, TATemplate
import logging
import sys
import os


class CommandFormulaAdd(Command):

     def _parse(self, parser):
        parser.add_argument('name', type=str, help='name of the formula to add')
        parser.add_argument('formula', type=str, help='formula to add')
        parser.add_argument('signature', type=str, help='a list of process names and their state spaces (e.g. p1:ss1,p2:ss2 )')
        
        
     def _do(self, args):

        signature = {}
        for item in args.signature.split(","):
            name, ss_name = item.split(":")
            signature[name] = ss_name

        Model.Instance().add_formula(args.name, args.formula, signature)
        
class CommandFormulaDel(Command):

     def _parse(self, parser):
        parser.add_argument('name', type=str, help='name of the formula to delete')

     def _do(self, args):
        Model.Instance().delete_formula(args.name)
        print 'The formula "' + args.name + '" has been successfully deleted'
		
class CommandFormulaInfo(Command):

     def _parse(self, parser):
        parser.add_argument('name', type=str, nargs="?", help='print all the formulas')   
        parser.add_argument('proc_name', type=str, help='process name')
        parser.add_argument('ta_template', type=str, help='timed automaton template')   

        
     def _do(self,args):

        ta_template = Model.Instance().get_ta(args.ta_template)
        ta = TA(args.proc_name, ta_template)

        if args.name:
            formula = Model.Instance().get_formula(args.name)
            

            print "%s : %s" % (args.name, formula.to_uppaal(ta))
        else:
            print "Formulas:"
            for name, curr in Model.Instance().get_formulas().iteritems():
                print "%s : %s" % (name, curr.to_uppaal(ta))
