from jamc.commands import Command
from jamc.models import Model

from java2ta.translator.models import KnowledgeBase
from java2ta.ir.shortcuts import get_time_variables, check_now_assignments

class CommandTimeNowMethod(Command):

    def _parse(self, parser):
        parser.add_argument('method_name', type=str, help='the fully-qualified name of a method that returns the current time')

    def _do(self, args):

#        now_method = Model.Instance().get_method(args.method_name)
        fq_method_name = args.method_name
        class_fqn, method_name = fq_method_name.rsplit(".", 1)
#        now_method = KnowledgeBase.get_methdo(args.method_name)

#        class_fqn = m.klass.fqname
#        method_name = m.name

        # at the moment we don't consider the case that the now method is one of the analyzed methods
#        (return_dt, parameters, method_env, interpretation) = KnowledgeBase.get_method(class_fqn, method_name)
        KnowledgeBase.set_now_method(class_fqn, method_name)

    def undo(self, state):
        """
        call unset_now_method
        """
        pass


class CommandCheckTimestamps(Command):

    def _parse(self, parser):
        parser.add_argument('method_name', type=str, help='the name of an imported method')


    def _do(self, args):

        print "TODO: refactor this"
##        target_method = Model.Instance().get_method(args.method_name)
##
##        class_fqn = target_method.parent.fqname
##        method_name = target_method.name
##
##        now_methods = KnowledgeBase.get_now_methods()
##
##        log.debug("Now methods: %s" % now_methods)
##
##        var_timestamps = get_time_variables(target_method) #, now_methods)
##        
##        log.debug("Time variables: %s" % var_timestamps)
##    
##        for var_name, nodes in var_timestamps.items():
##            is_relative = ...
##            is_now_timestamp = check_now_assignments(nodes, now_methods)
##
##
##            KnowledgeBase.add_timestamp(class_fqn, method_name, var_name, is_relative, is_now_timestamp)
## TODO refactor

class CommandTimeInfo(Command):

    def _parse(self, parser):
        parser.add_argument('method_name', type=str, help='the name of an imported method')


    def _do(self, args):

        target_method = Model.Instance().get_method(args.method_name)

        class_fqn = target_method.parent.fqname
        method_name = target_method.name

        now_methods = KnowledgeBase.get_now_methods()

        time_variables = get_time_variables(target_method) #, now_methods)
 
        print "Time variables:"
        for var, time_type in time_variables.items():
            is_relative = ("time_type" == "Duration")
            KnowledgeBase.add_timestamp(class_fqn, method_name, var, is_relative)
            print("- %s : %s" % (var, time_type))


