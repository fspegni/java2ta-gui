from jamc.commands import Command
from jamc.models import Model
from jamc.errors import CommandError
from jamc.request import Request

from java2ta.translator.models import KnowledgeBase
from java2ta.abstraction.models import Boolean, Integer, Object, Iterator, PredicateParser, AbsString

from contracts import contract
import copy

import logging

log = logging.getLogger(__name__)

class CommandMethodAdd(Command):

#    _previous_state = {}

    _args = None

    description = 'Imports a new method from the current project.'

    def _parse(self, parser):
        parser.add_argument('method_name',        type=str,                help='identifying name of the method in this program\'s execution (can be different from the name in the signature)')
        parser.add_argument('file_name',        type=str,                help='name (with the relative path) of the file containing the class the method belongs to')
        parser.add_argument('class_name',        type=str,                help='name of the class the method belongs to')
        parser.add_argument('method_signature',    type=str, nargs='?',    help='actual name of the method as seen in the code, if missing by default is the value of method_name')

    def _set_previous_state(self, previous_state, args):
        previous_state['name'] = args.method_name

    def _do(self, args):

        curr_project = Model.Instance().get_curr_project()

        if not curr_project:
            raise CommandError("You must open a project first")

        Model.Instance().add_method(args.method_name, args.file_name, args.class_name, args.method_signature or args.method_name)

    def undo(self, state):
        Model.Instance().delete_method(state['name'])

class CommandMethodDel(Command):

    _previous_state = {}

    _args = None

    description = 'Deletes a method.'

    def _parse(self, parser):
        parser.add_argument('name', type=str, help='name of the method to delete')

    def _set_previous_state(self, previous_state, args):
        previous_state['method_name'] = args.name
        previous_state['method'] = copy.copy(Model.Instance().get_method(args.name))

    def _do(self, args):
        Model.Instance().delete_method(args.name)

    def undo(self, state):
        Model.Instance().undo_delete_method(state['method_name'], state['method'])

class CommandMethodList(Command):

    _args = None

    description = 'Shows all the imported methods, or (if specified) shows all the methods belonging to a given class (not necessarily imported).'

    def _parse(self, parser):
        parser.add_argument('classpath',        type=str, nargs='*', help='takes zero or two arguments: file name (relative path in project) and class name (which methods will be shown; if this is missing, all the imported methods will be shown')
        parser.add_argument('-v', '--verbose',    action='store_true', help='shows more detailed information, this option has no effect if the classpath is specified too')

    def _set_previous_state(self, previous_state, args):
        return

    def _do(self, args):
        output = '\n\n'
        if args.classpath:
            if len(args.classpath) != 2:
                raise CommandError('Wrong number of arguments')
            project = Model.Instance().get_curr_project()
            file_name = args.classpath[0]
            class_name = args.classpath[1]
            methods = Request.Instance().get_methods_from_class_name(project, file_name, class_name)
            output += '************* Methods in class "' + class_name + '" *************\n'
            for method_name in methods:
                output += '\n- ' + method_name
        else:
            methods = Model.Instance().get_methods()
            output += '************* Methods list *************\n'
            if args.verbose:
                output += '\n-----------------------'
            for method_name, method in methods.items():
                if args.verbose:
                    output += '\nMethod name: ' + method_name
                    output += '\n' + Model.Instance().method_to_string(method)
                    output += '\n\n-----------------------'
                else:
                    output += '\n- ' + method_name
        return output

    def undo(self, state):
        return

class CommandMethodRen(Command):

    _previous_state = {}

    _args = None

    description = 'Renames a method.'

    def _parse(self, parser):
        parser.add_argument('old_name', type=str, help='current name of the method to rename')
        parser.add_argument('new_name',    type=str, help='new name of the method')

    def _set_previous_state(self, previous_state, args):
        previous_state['old_name'] = args.old_name
        previous_state['new_name'] = args.new_name

    def _do(self, args):
        Model.Instance().rename_method(args.old_name, args.new_name)

    def undo(self, state):
        Model.Instance().rename_method(state['new_name'], state['old_name'])

class CommandMethodInterpret(Command):

    description = "Add an SMT interpretation for the method"

    TYPE_CLASSES = {
        "bool": Boolean,
        "Boolean": Boolean,
        "int": Integer,
        "Integer": Integer,
        "long": Integer,
        "Long": Integer,
        "void": Boolean, # TODO this should be fixed, providing a custom type
        "Object": Object,
        "Iterator": Iterator,
        "String": AbsString,
    }

    def _parse(self, parser):
        parser.add_argument('method_name', type=str, help='the method for which the interpretation is given')
        parser.add_argument('type', type=str, help='the Java returned type for the method')
        parser.add_argument('parameters', type=str, help='a list of Java types of the method formal parameters')
        parser.add_argument('env', type=str, help='a list of variables read and/or modified by the method')
        parser.add_argument('interpretation', type=str, nargs='?', help='a comma separated list of SMT declarations and assertions that constitute the interpretation of the method')

    def _do(self, args):

##        m = Model.Instance().get_method(args.method_name)
##
##        class_fqn = m.klass.fqname
##        method_name = m.name
##
##        log.debug("Interpretation for method %s: %s" % (m.name, args.interpretation))
        (class_fqn, method_name) = args.method_name.rsplit(".", 1)
        interpretation = args.interpretation
        method_type = self.get_method_type(args.type)
        parameters_signature = self.get_parameters_signature(args.parameters)
        method_env = self.get_method_env(args.env)

        pp = PredicateParser()
        pred = pp.parse(interpretation)

        log.debug("Parsed predicate for interpretation of method '%s': %s" % (args.method_name, pred))
        log.debug("Environment of interpreted method: %s" % method_env)
        log.debug("Type fo interpreted method: %s" % method_type)
        log.debug("Signature of interpreted method: %s" % parameters_signature)
        KnowledgeBase.add_method(class_fqn, method_name, (method_type, parameters_signature, method_env, pred)) # TODO replace: [], dummy and Boolean() with actual values

    @contract(interpretation_text="string",returns="list(string)")
    def get_interpretation(self, interpretation_text):

        res = []
        assertions = interpretation_text.split(",")
        for a in assertions:
            res.append("(assert %s)" % a)

        return res
 
    def get_method_env(self, env):
        """
        The env parameter has the following form:
        (var1,var2,...,varN)
        Each var is a var of the environment.
        """
        return env.strip("()").split(",")
          
    def get_method_type(self, type_text):
        # TODO perhaps here we can use DataTypeFactory.the_factory.from_fqn(...) ?
        if type_text not in self.TYPE_CLASSES:
            raise ValueError("Method type not recognized. Available types: %s" % self.TYPE_CLASSES.keys())
    
        type_class = self.TYPE_CLASSES[type_text]

        return type_class()
    
    @contract(parameters="string", returns="list(string)")
    def get_parameters_signature(self, parameters):
        """
        The parameters string has the following form:
        (type1,type2,...,typeN)
        """
        return parameters.strip("()").split(",")

class CommandMethodInterpretation(Command):

    def _parse(self, parser):
        parser.add_argument('method_name', type=str, nargs="?", help='identifying name of the method in this program\'s execution (can be different from the name in the signature)')


    @contract(class_name="string", method_name="string")
    def _print_interpretation(self, class_name, method_name):

        if not KnowledgeBase.has_method(class_name, method_name):
            raise CommandError("The passed method (%s.%s) has no interpretation" % (class_name, method_name))

        datatype, signature, env, interpretation = KnowledgeBase.get_method(class_name, method_name)

        print "Class: %s - Method: %s" % (class_name, method_name)
        print "  - datatype      : %s" % datatype
        print "  - signature     : %s" % signature
        print "  - environment   : %s" % env
        print "  - interpretation: %s" % interpretation

    def _do(self, args):

        methods = []
        if args.method_name:
            class_name = ""
            parts = args.method_name.rsplit(".", 1)
            if len(parts) > 1:
                class_name = parts[0]
                method_name = parts[1]

            methods = [ (class_name, method_name) ]
        else:
            methods = KnowledgeBase.get_methods()

        for (c,m) in methods:
            self._print_interpretation(c,m)
