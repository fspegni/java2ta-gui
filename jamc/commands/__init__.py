from jamc.config import *
from jamc.errors import CommandError

import argparse
import shlex
import traceback
from abc import ABCMeta, abstractmethod


# da questa classe estendono tutti i comandi
class Command:

    __metaclass__ = ABCMeta

    # contiene alcuni valori di interesse che descrivono lo stato precedente all'esecuzione,
    # la natura di questi valori dipende dal comando stesso ed e' per questo che si usa un dizionario,
    # questo dizionario ha un'utilita' temporanea perche' alla fine dell'esecuzione del comando
    # il suo contenuto verra' copiato nella pila undo_stack, e servira' appunto a recuperare lo stato precedente in caso di undo
    #_previous_state = {}


    # contiene gli argomenti passati al comando, estrapolati dal metodo __parse, utilizzati durante l'esecuzione del comando (metodo _do)
    # ed eventualmente nell'impostazione dello stato precedente (metodo _set_previous_state),
    # prima dell'esecuzione del comando il suo valore viene reimpostato a None per evitare
    # eventuali riferimenti sbagliati da esecuzioni precedenti
    #_args = None


    # descrizione del comando che verra' visualizzata nell'helper
    description = None


    # metodo richiamato dal metodo __run della classe UI per eseguire il comando con gli opportuni parametri passati:
    # - i parametri vengono interpretati dal metodo _parse
    # - viene salvato lo stato (che verra' restituito alla fine) tramite il metod _set_previous_state
    # - viene eseguito il comando tramite il metodo _do
    # - viene restituito lo stato precedente all'esecuzione
    # questo metodo non fa controlli sulla correttezza dei parametri e non gestisce le eccezioni generate,
    # cio' viene fatto dal metodo __run della classe UI

    def __init__(self, trace=False):
        self.trace = trace

    def run(self, command_name, params):
        parser = argparse.ArgumentParser(description = self.description, prog = command_name)
        self._parse(parser)

        # se params = None (non sono stati inviati parametri), di default il parser sostituisce params con sys.argv,
        # ma a noi non cambia nulla perche' abbiamo svuotato sys.argv all'avvio del programma

        previous_state = {}

        try:
            args = parser.parse_args(params)

            self._set_previous_state(previous_state, args)
            res = self._do(args)
        except Exception, e:
            if self.trace:
                traceback.print_exc()
            raise CommandError("Error executing command '%s': %s. Details: %s. Error arguments: %s" % (command_name, type(e), e.message, e.args))
        except SystemExit:
            # this is thrown if --help is passed; just ignore it
            pass

        return previous_state


    # definisce gli argomenti che possono essere passati da linea di 
    # comando e li aggiunge alla variabile parser, questi poi verranno 
    # interpretati e inseriti nella variabile _args dal metodo run
    def _parse(self, parser):                                
        pass


    def _set_previous_state(self, previous_state, args):    
        """
        salva lo stato nel dizionario _previous_state prima di eseguire 
        il comando, per farlo puo' eventualmente avvalersi di _args 
        perche' il parsing e' gia' stato fatto
        """
        pass


    @abstractmethod
    def _do(self, args):                                    
        """
        esegue le azioni che compongono il comando
        """
        pass


    def undo(self, state):                                    
        """
        ripristina lo stato passato come parametro
        """
        pass
