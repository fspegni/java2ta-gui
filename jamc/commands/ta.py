from jamc.commands import Command
from jamc.models import Model
from java2ta.ta.models import NTA, TA, TATemplate, Edge, TimeEdge, SleepEdge, Variable, Int, filter_locations_by_pc, get_incoming_edges, get_outgoing_edges
from java2ta.ta.views import TADisplay, Uppaal
from java2ta.abstraction.models import Imply, And, Or, Tautology
from java2ta.abstraction.shortcuts import DataTypeFactory
from java2ta.translator.shortcuts import method_to_ta_template
from java2ta.translator.models import KnowledgeBase, FreshNames, PC
from jamc.errors import CommandError
from contracts import contract, check
import tempfile
import os
import logging
import subprocess

log = logging.getLogger()

class CommandTAGraph(Command):

    _previous_state = {}

    _args = None

    description = 'Opens the file containing the given timed automaton.'

    def _parse(self, parser):
        parser.add_argument('name', type=str, help='name of the timed automaton to open as a graph')
        parser.add_argument('--engine', type=str, default=TADisplay.DOT_ENGINE_CHOICES[0], help="name of the engine to use to layout the graph", choices=TADisplay.DOT_ENGINE_CHOICES)
        parser.add_argument('--ext', type=str, default="svg", help="extension of the output file")
        parser.add_argument('--out', type=str, nargs="?", help='name of the output graph (and file)')

    def _set_previous_state(self, previous_state, args):
        return

    def _do(self, args):
#        Model.Instance().ta_graph(args.name)

        curr_ta = Model.Instance().get_ta_info(args.name)

        if curr_ta is None:
            raise CommandError("TATemplate not found")

        (method_name, ss_name, ss, ta_template) = curr_ta

        legend = Model.Instance().build_legend(ss)

        d = TADisplay(ta_template, legend, type=args.ext, engine=args.engine, name=args.out)
#        gv.save(ta.name)
        d.open()

    def undo(self, state):
        return

class CommandTAList(Command):

	_previous_state = {}

	_args = None

	description = 'Shows all the created FSAs.'

	def _parse(self, parser):
		group = parser.add_mutually_exclusive_group()
		group.add_argument('-m', '--method_name',	type=str,				help='name of the method which FSAs will be shown')
		group.add_argument('-p', '--method_path',	type=str, nargs='+',	help='path of the method which FSAs will be shown (project, file, class, method)')
		parser.add_argument('-v', '--verbose',		action='store_true',	help='shows more detailed info')

	def _set_previous_state(self, previous_state, args):
		return

	def _do(self, args):
		all_ta = Model.Instance().get_all_ta()
		output = '\n\n************* Timed Automata list *************\n'
		if args.verbose:
			output += '\n-----------------------'
		for ta_name, (method_name, statespace_name, _) in all_ta.items():
			if args.verbose:
				method = Model.Instance().get_method(method_name)
				statespace = Model.Instance().get_statespace_attributes(statespace_name)
				output += '\nTATemplate name: ' + ta_name
				output += '\n\n*** Method: ' + method_name
				output += '\n' + Model.Instance().method_to_string(method)
				output += '\n\n*** State space: ' + statespace_name
				output += '\n' + Model.Instance().statespace_to_string(statespace)
				output += '\n\n-----------------------'
			else:
				output += '\n- ' + ta_name
		return output

	def undo(self, state):
		return

class CommandTAMake(Command):

    _previous_state = {}

    _args = None

    description = 'Generates a file containing the timed automaton of a given state space associated to a given method method.'

    def _parse(self, parser):
        parser.add_argument('method_name',    type=str, help='name of the method from which the timed automaton will be created')
        parser.add_argument('ss_name',        type=str, help='name of the state space from which the timed automaton will be created')
        parser.add_argument('ta_name',        type=str, help='name of the timed automaton to create')
        parser.add_argument('--all_urgent', action="store_true", default=False, help="specify this if all the TATemplate locations should be tagged as urgent")

    def _set_previous_state(self, previous_state, args):
        previous_state['ta_name'] = args.ta_name

    def _do(self, args):
        method = Model.Instance().get_method(args.method_name)
        ss = Model.Instance().get_statespace(args.ss_name)
        ta_template = method_to_ta_template(method, ss)
        Model.Instance().set_ta_info(args.ta_name, (args.method_name, args.ss_name, ss, ta_template))

        ta_template.add_clock_variable("C_PROG")
        ta_template.add_variable(Variable("T_PROG_MAX", Int()))

        if args.all_urgent:
            # we consider all locations as urgent
            for loc in ta_template.locations:
                loc.set_urgent()

        if not args.all_urgent:
            # a finer analysis: 
            # - every TimeEdge, has a non-urgent source location
            # (we assume all source locations are initialized to *non* urgent, by previous loop)
            for edge in ta_template.edges:
                if isinstance(edge, TimeEdge):
                    edge.source.set_urgent(False)
                    for in_edge in edge.source.incoming:
                        in_edge.add_reset("C_PROG")

                    edge.source.add_invariant("C_PROG < T_PROG_MAX")
                elif isinstance(edge, SleepEdge):
                    edge.source.set_urgent(False)
                    for in_edge in edge.source.incoming:
                        in_edge.add_reset("C_PROG")
                    source_inv = "C_PROG <= %s" % edge.sleep_time
                    edge.source.add_invariant(source_inv)
##                    for e in edge.source.incoming:
##                        e.add_constraint(source_inv)
                    edge.add_constraint("C_PROG >= %s" % edge.sleep_time)

    def undo(self, state):
        # TODO remove the files associated to the timed automaton
        Model.Instance().remove_ta(state['ta_name'])


class CommandTAInfo(Command):

    description = "Prints useful information on the specified timed automaton"

    def _parse(self, parser):
        parser.add_argument('-v', '--verbose',		action='store_true',	help='shows more detailed info')
        parser.add_argument('name', type=str, help='name of the timed automaton to open as a graph')
        

    def _do(self,args):
        if args.verbose:
            curr_ta = Model.Instance().get_ta_info(args.name)
        
            if curr_ta is None:
                raise CommandError("TATemplate not found")

            (method_name, ss_name, ss, ta_template) = curr_ta
            output = '\n\nList of the location names:'      
            output_edges = ""

            for ta_location in sorted(ta_template.locations, key=lambda loc: loc.name):
                output += '\n'+str(ta_location)
                for ta_edge in sorted(ta_location.outgoing, key=lambda e: e.target.name):
                    guard = ""
                    if ta_edge.guard:
                        guard = "[%s]" % ta_edge.guard

                    output_edges += "\n%s -%s-> %s" % (ta_edge.source, guard, ta_edge.target)

            output += '\n\nList of the edges:' + output_edges
            output += '\n\nList of the clock variables:'

            for ta_clock_variable in ta_template.clock_variables:		
                output += '\n'+str(ta_clock_variable)
          
            output += '\n\nList of the variables:'

            for ta_variable in ta_template.variables:	
                output += '\n'+str(ta_variable)

            print output,"\n"

        if args.name:
            curr_ta = Model.Instance().get_ta_info(args.name)
        
            if curr_ta is None:
                raise CommandError("TATemplate not found")

            (method_name, ss_name, ss, ta_template) = curr_ta
        
            num_locs = len(ta_template.locations)
            num_edges = len(ta_template.edges)
            num_clocks = len(ta_template.clock_variables)
            print "Locations: %s. Edges: %s. Clock variables: %s." % (num_locs, num_edges, num_clocks)
        

class CommandTAOptimize(Command):

    def _parse(self, parser):
        parser.add_argument('name', type=str, help='name of the timed automaton to optimize')
    

    def _do(self,args):

        (method_name, ss_name, ss, ta_template) = Model.Instance().get_ta_info(args.name)

        assert isinstance(ta_template, TATemplate)
    
        flag=True
    
        opti_father=UnifyInFather(ta_template)
        opti_son=UnifyInSon(ta_template) 
        
        while(flag):       
            flag=False
            list_loc=list(ta_template.locations)
            list_loc.sort()
            i=0
            while(len(list_loc) != 0):
                loc=list_loc.pop()
                if(opti_father.can_apply(loc)):
                        flag=True
                        im_your_father=list(loc.incoming)[0]
                        im_your_father=im_your_father.source
                        opti_father.do_apply(loc,im_your_father,ta_template)
                        log.debug("This location has been unified in father: %s" % loc.name)
                        im_your_father.name=opti_father.edit_label_newloc(loc,im_your_father)
                        ta_template.locations.remove(loc)
        flag=True
        while(flag):
            flag=False
            list_loc=list(ta_template.locations)
            list_loc.sort()
            i=0
            while(len(list_loc)!=0):
                loc=list_loc.pop()
                if(opti_son.can_apply(loc)):
                        flag=True
                        im_your_son=list(loc.outgoing)[0]
                        im_your_son=im_your_son.target
                        opti_son.do_apply(loc,im_your_son,ta_template)
                        log.debug("This location has been unified in son: %s" % loc.name)
                        im_your_son.name=opti_son.edit_label_newloc(loc,im_your_son)
                        ta_template.locations.remove(loc)


class GraphRule(object):
    LBL_SIZE=0

    def __init__(self, ta):
        self.ta = ta
        self.LBL_SIZE=10

    def can_apply(node_a, node_b):
        raise ValueError("Must override this method")

    def do_apply(node_a, node_b):
        raise ValueError("Must override this method") 
    
    def edit_label_newloc(self,loc_to_del,loctoupd):
        state=str((loctoupd.name.split("@"))[0])
        version_loc_del=self.create_version(loc_to_del)
        version_loc_upd=self.create_version(loctoupd)
        vers_del=self.check_version(version_loc_del)
        vers_upd=self.check_version(version_loc_upd)
        vers_del_first=str(vers_del[0])
        vers_del_last=str(vers_del[1])
        vers_upd_first=str(vers_upd[0])
        vers_upd_last=str(vers_upd[1])
        if(vers_del_last == "None" and vers_upd_last=="None"):
           result=self.which_version(vers_del_first,vers_upd_first)
           label=state+"@["+str(result[0])+"-"+str(result[1])+"]"
        if(vers_del_last == "None" and vers_upd_last!="None"):
            result_first=(self.which_version(vers_del_first,vers_upd_first))[0]
            label=state+"@["+str(result_first)+"-"+str(vers_upd_last)+"]"
        if(vers_del_last != "None" and vers_upd_last=="None"):
            result_first=(self.which_version(vers_del_first,vers_upd_first))[0]
            label=state+"@["+str(result_first)+"-"+str(vers_del_last)+"]"
        if(vers_del_last != "None" and vers_upd_last != "None"):
            result_first=(self.which_version(vers_del_first,vers_upd_first))[0]
            result_last=(self.which_version(vers_del_last,vers_upd_last))[1]
            label = state + "@[" + str(result_first)+"-"+str(result_last)+"]"
        return label

    def which_version(self,ver1,ver2):
            result=None
            if(len(ver1)>len(ver2)):
                result =[ver2,ver1]
            if(len(ver1)<len(ver2)):
                result= [ver1,ver2]
            if(len(ver1)==len(ver2)):
               result= self.ricorsive_check_version(ver1,ver2,0)
            return result
                    
    def ricorsive_check_version(self,ver1,ver2,a):
        if ver1[a]==ver2[a]:
            if(a<len(ver1)):
                a=a+1
                return self.ricorsive_check_version(ver1,ver2,a)
            else:
                return [ver2,ver1]
        else:
            if(ver1[a] > ver2[a]):
                return [ver2,ver1]
            if(ver1[a] < ver2[a]):
                return [ver1,ver2]
 

    def create_version(self,loc):
        
        try:
            version=str((loc.name.split("@"))[1])
        except IndexError:
            version=str(loc.name)
        version=str(version.replace("[",""))
        version=str(version.replace("]",""))
        return version
    
    def check_version(self,ver):
        try:
            first_ver=str((ver.split("-"))[0])
        except IndexError:
            first_ver=str((ver))
        try:
            last_ver=str((ver.split("-"))[1])
        except IndexError:
            last_ver=None
        a=[first_ver,last_ver]
        return a
  
 
    @contract(ta="is_ta_template")
    def createnew_edge(self,loc_son,im_your_father,label_father,label_son,ta):
        if (label_father !=None and label_son !=None):
            tot_label= "%s / %s" %(label_father,label_son)
        else:
            if(label_father == None):
                tot_label= "%s" %label_son
            if(label_son == None):
                tot_label= "%s" %label_father
        if (len(tot_label)> (2*self.LBL_SIZE + 3)):
            tot_label = tot_label[:self.LBL_SIZE] + "..." +tot_label[(len(tot_label))-(self.LBL_SIZE):(len(tot_label))]
        new_edge=Edge(im_your_father,loc_son,tot_label)
        new_edge.__str__()
        ta.edges.add(new_edge)
        return new_edge


class UnifyInFather(GraphRule):

    def can_apply(self, this_loc):
        if(len(this_loc.incoming)!=1):
            return False
        father_edge = list(this_loc.incoming)[0]
        state_fath=father_edge.source.name.split("@")
        state_son=this_loc.name.split("@")
        if(state_fath[0]!=state_son[0]):
            return False
        return True

    @contract(ta="is_ta_template")
    def do_apply(self,this_loc,im_your_father,ta):
        edge_to_fath=this_loc.incoming.pop()
        label_father = self.updatefather_noson(this_loc,edge_to_fath,im_your_father,ta)
        for edge in this_loc.outgoing:
            loc_son=edge.target
            label_son = self.updateson_noson(this_loc,edge,loc_son,ta)
            new_edge = self.createnew_edge(loc_son,im_your_father,label_father,label_son,ta)
            loc_son.incoming.add(new_edge)
            im_your_father.outgoing.add(new_edge)
     
     #	metodo per aggiornare la location padre che subisce variazione    
    def updatefather_noson(self,loc_to_del,edge_to_fath,im_your_father,ta_template):
        label1=edge_to_fath.label
        im_your_father.outgoing.discard(edge_to_fath)
        ta_template.edges.discard(edge_to_fath)
        return label1
    
    
#	uguale sopra solo che per il figlio     	
    def updateson_noson(self,this_loc,edge_to_son,loc_son,ta_template):
        label2=edge_to_son.label
        loc_son.incoming.discard(edge_to_son)
        ta_template.edges.discard(edge_to_son)
        return label2

class UnifyInSon(GraphRule):
    
    def can_apply(self, this_loc):	
        if(len(this_loc.outgoing)!=1):
            return False
        son_edge = list(this_loc.outgoing)[0]
        state_son=son_edge.target.name.split("@")
        state_fath=this_loc.name.split("@")
        if(state_fath[0]!=state_son[0]):
            return False
        return True
    
    def do_apply(self,this_loc,son,ta):
        edge_to_son=this_loc.outgoing.pop()
        label_son=self.updateson_nofather(this_loc,edge_to_son,son,ta)
        for edge in this_loc.incoming:
            loc_fath=edge.source
            label_father = self.updatefather_nofather(this_loc,edge,loc_fath,ta)
            new_edge = self.createnew_edge(son,loc_fath,label_father,label_son,ta)
            loc_fath.outgoing.add(new_edge)	  	
            son.incoming.add(new_edge)
        
# metodo per aggiornare la location padre che subisce variazione    	
    def updatefather_nofather(self,loc_to_del,edge_to_fath,im_your_father,ta_template):
        label1=edge_to_fath.label
        im_your_father.outgoing.discard(edge_to_fath)
        ta_template.edges.discard(edge_to_fath)
        return label1
    	
    	
#	uguale sopra solo che per il figlio     	
    def updateson_nofather(self,this_loc,edge_to_son,loc_son,ta_template):
        label2=edge_to_son.label
        loc_son.incoming.discard(edge_to_son)
        ta_template.edges.discard(edge_to_son)
       	return label2
  
   
class CommandTAInterpret(Command):

    description = "Build an SMT interpretation for the timed automaton"

    INFINITY_DIST = -1

    def _parse(self, parser):
        parser.add_argument('name', type=str, help='name of the timed automaton to interpret')
        parser.add_argument('parameters', type=str, help='formal parameters for the automaton seen as a method')
        parser.add_argument('globals', type=str, help='global variables accessed by the automaton seen as a method')


    def _do(self,args):
        
        (method_name, ss_name, ss, ta_template) = Model.Instance().get_ta_info(args.name)

        assert isinstance(ta_template, TATemplate)
        
        reachability_matrix = self.build_reachability_matrix(ta)
        global_vars = self.get_env(args.globals)
        pred = self.build_predicate(ss, ta, reachability_matrix, global_vars)

        method = Model.Instance().get_method(method_name)
        class_name = method.class_name

        # note that method.name != method_name (in general): method_name is the method identifier
        # used by the tool commands (and the Model instance); method.name is the name of the actual
        # Java method
        #log.debug("Method to be interpreted: %s" % method.ast)
        smt_method_type = DataTypeFactory.the_factory().from_fqn(method.type)
        parameters_signature = self.get_parameters_signature(args.parameters)
        interpretation = (smt_method_type, parameters_signature, global_vars, pred)
        KnowledgeBase.add_method(class_name, method.name, interpretation)
        print "%s : %s" % (method.fqname, pred)
    

    @contract(parameters="string", returns="list(string)")
    def get_parameters_signature(self, parameters):
        return parameters.strip("()").split(",")


    @contract(variables="string", returns="list(string)")
    def get_env(self, variables):
        return variables.strip("()").split(",")

    @contract(ss="is_state_space", ta_template="is_ta_template", reachability_matrix="dict(string:dict(string:int))", global_vars="list(string)", returns="is_predicate")
    def build_predicate(self, ss, ta_template, reachability_matrix, global_vars):

        init_locations = self.get_init_loc(ta_template)
        conjuncts = []

        for loc_start in init_locations:
            pred_start = And(*loc_start.data) # loc_start.data is a tuple of predicates that hold in the starting location
            check("is_predicate", pred_start)

            disjuncts = []

            for loc_final in ta_template.final_locations:
                distance = reachability_matrix[loc_start.name][loc_final.name]
                #print "%s --(%s)--> %s" % (loc_start, distance, loc_final)  
                if distance >= 0:
                    pred_final = And(*loc_final.data) # loc_final.data is a tuple of predicates that hold in the final location
                    check("is_predicate", pred_final)
                    disjuncts.append(pred_final.primed(var_names=global_vars))

            if len(disjuncts) > 0:
                if len(disjuncts) == 1:
                    pred = Imply(pred_start, disjuncts[0])
                else:
                    pred = Imply(pred_start, Or(*disjuncts)) # pred := pred_start -> pred_final
                conjuncts.append(pred)

        assert len(conjuncts) > 0
        res = And(*conjuncts)
            
        return res

                    
    @contract(ta_template="is_ta_template", returns="dict(string:dict(string:int))")
    def build_reachability_matrix(self, ta_template):
        matrix = {}

        initial_locations = self.get_init_loc(ta_template)

        #print "initial locations: %s" % initial_locations
        # store the initial distance
        for loc in ta_template.locations:
            matrix[loc.name] = {}
            for target in ta_template.locations:
                initdist = CommandTAInterpret.INFINITY_DIST #-1
                if loc == target:
                    initdist = 0

                matrix[loc.name][target.name] = initdist

        for loc_start in initial_locations:
            dist_vect = self.dijkstra(loc_start, ta)
            #print "loc start: %s -> dist vect: %s" % (loc_start, dist_vect,)
            for reachable, distance in dist_vect.iteritems():
                matrix[loc_start.name][reachable] = distance
                
        return matrix

    @contract(ta_template="is_ta_template", returns="list(is_location)") 
    def get_init_loc(self,ta_template):
        init_loc=[]
        for item in list(ta_template.initial_loc.outgoing):
            init_loc.append(item.target)
        return init_loc


    @contract(locstart="is_location", ta_template="is_ta_template", returns="dict(string:int)") 
    def dijkstra(self, locstart, ta_template):
        #initdist= -1

        # custom comparison function for distance: make 'INFINITY_DIST' the greatest distance value
        dist_key = lambda d: (-1,d) if d != CommandTAInterpret.INFINITY_DIST else (1,1)
        dist={}
        graph=[]
        
        # at the beginning, only locstart is reachable with 0 steps, all the others are unreachable
        for loc in ta_template.locations:
            graph.append(loc.name)
            dist[loc.name] = CommandTAInterpret.INFINITY_DIST

        dist[locstart.name] = 0

        # iterate in the greedy way
        #print "building dijkstra for loc start: %s" % locstart
        while len(graph)!=0:
            
            # find the closest node in the graph and remove it
            closest_node_name = graph[0]
            closest_node_dist = dist[graph[0]]
            for node_name in graph[1:]:
                node_dist = dist[node_name]
                if dist_key(node_dist) < dist_key(closest_node_dist):
                    closest_node_name = node_name
                    closest_node_dist = node_dist

            #print "closest node: name=%s, dist=%s" % (closest_node_name, closest_node_dist)
             
            graph.remove(closest_node_name)

            # iterate over the neighbors of the closest node, and compute a (reduced) distance from
            # the start location to them
            u = ta_template.get_location(closest_node_name)
            #print "found location: %s" % u
            #print "found neighbors: %s" % (u.neighbors)
            #print "dist pre: %s" % (dist,)
            for v in u.neighbors:

                new_dist = CommandTAInterpret.INFINITY_DIST #initdist
                if dist[u.name] != CommandTAInterpret.INFINITY_DIST: #initdist
                    new_dist = dist[u.name] + 1

                if dist[v.name] == CommandTAInterpret.INFINITY_DIST or (new_dist != CommandTAInterpret.INFINITY_DIST and new_dist < dist[v.name]):
                    dist[v.name] = new_dist
        
            #print "dist post: %s" % (dist,)
        return dist

   	         

class CommandTAConstraint(Command):
    
    def _parse(self, parser):

        parser.add_argument('ta_name', type=str, help='name of the TA template to which the constraint should be added')
        parser.add_argument('locations', type=str, help='specify the set of locations whose execution time is subject to the given bound')
        parser.add_argument('bound', type=str, help='a comparison (e.g. "< foo" or ">= fie")')

    def _do(self, args):

        try:
            curr_ta = Model.Instance().get_ta_info(args.ta_name)
        
            if curr_ta is None:
                raise CommandError("TATemplate not found")

            (method_name, ss_name, ss, ta_template) = curr_ta
        except Exception, e:
            raise CommandError("Error looking for TA template '%s': %s" % (args.ta_name, e))


        locations_parts = args.locations.split("-")
        locations_parts = map(lambda s: s.strip(), locations_parts)

        first_pc = PC(locations_parts[0])
        last_pc = first_pc

        num_parts = len(locations_parts)
        if num_parts == 0 or num_parts > 2:
            raise ValueError("Expected an interval with one value or one upper and one lower bounds. Passed: %s" % locations_parts)
        elif num_parts == 2:
            last_pc = PC(locations_parts[1])
            if first_pc.num_levels != last_pc.num_levels:
                raise ValueError("Expected interval of locations with different PCs at the same level. Passed: %s and %s" % (first_pc, last_pc))

            if first_pc > last_pc:
                raise ValueError("Expected interval with lower-bound less than upper-bound. Passed: %s (lower bound) and %s (upper bound)" % (first_pc, last_pc))
        
        # add the constraint to the NTA
        locations = filter_locations_by_pc(ta_template, first_pc, last_pc)

        if len(locations) > 0:
            ta_template.add_clock_variable("C_CONSTRAINT") # TODO understand when new clock variables must be created
            log.debug("Locations: %s" % locations)
            incoming = get_incoming_edges(locations)
            log.debug("Incoming: %s" % incoming)
            for e in incoming:
                e.add_reset("C_CONSTRAINT")
            for l in locations:
                l_inv = "C_CONSTRAINT %s" % args.bound
                l.add_invariant(l_inv)
                l.set_urgent(False) # TODO check this has sense
                for e in l.incoming:
                    # for every incoming edge, add a guard identical to the invariant of the target
                    # state (only if the transition does not already reset the C_CONSTRAINT clock)
##                    #g = []
##                    #if e.guard:
##                    #    g.append(e.guard)
##                    #g.append(l_inv)
##                    #e.guard = " && ".join(g)
                    if "C_CONSTRAINT" not in e.reset:
                        e.add_constraint(l_inv)
## 
class CommandTASet(Command):

    def _parse(self, parser):

        parser.add_argument('ta_name', type=str, help='name of the variable to set')
        parser.add_argument('var', type=str, help='name of the variable to set')
        parser.add_argument('type', type=str, help='variable type')
        parser.add_argument('value', type=str, help='variable value')

 
    def _do(self, args):

        try:
            curr_ta = Model.Instance().get_ta_info(args.ta_name)
        
            if curr_ta is None:
                raise CommandError("TATemplate not found")

            (method_name, ss_name, ss, ta_template) = curr_ta
        except Exception, e:
            raise CommandError("Error looking for TA template '%s': %s" % (args.ta_name, e))

        new_var = Variable(args.var, args.type, args.value)
        log.debug("Set variable: %s" % new_var)
        ta_template.add_variable(new_var)
    
