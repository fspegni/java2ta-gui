from jamc.config import *
from jamc.commands import Command
from jamc.models import Model
from jamc.request import Request
from jamc.errors import ProjectNoneError, CommandError

class CommandProjClasses(Command):

    _previous_state = {}

    _args = None

    description = 'Shows all the classes in the current project.'

    def _parse(self, parser):
        parser.add_argument('-v', '--verbose',    action='store_true', help='shows more detailed information, this option has no effect if the classpath is specified too')

    def _set_previous_state(self, previous_state, args):
        return

    def _do(self, args):
        project = Model.Instance().get_curr_project()

        if project is None:
            raise CommandError("Must open a project before")

        classes = Request.Instance().get_classes_from_project(project)
        output = ""
#        output = '************* Classes in project "' + project.name + '" *************\n'
#        if args.verbose:
#            output += '\n-----------------------'
        for file_name, class_name in classes:
            output += "+ %s (%s)\n" % (class_name, file_name)
            if args.verbose:
#                output += '\nFile: ' + file_name + '\t\t Class: ' + class_name
                methods = Request.Instance().get_methods_from_class_name(project, file_name, class_name)
                for method_name in methods:
                    output += '|-- %s\n' % method_name
#                output += '\n\n-----------------------'
#            else:
#                output += '\n- ' + file_name + ' => ' + class_name
#        return output
        print output.strip()

    def undo(self, state):
        return

class CommandProjClean(Command):

    _previous_state = {}

    _args = None

    description = 'Closes and cleanup the current project from the server.'

    def _parse(self, parser):
        return parser

    def _set_previous_state(self, previous_state, args):
        project = Model.Instance().get_curr_project()

        if not project:
            raise CommandError("Must open a project first")

        previous_state['name'] = project.name
        previous_state['path'] = project.path
        previous_state['url'] = project.url

    def _do(self, args):
        Model.Instance().close_project(clean=True)

    def undo(self, state):
        print "This command cannot be undone"

class CommandProjClose(Command):

    _previous_state = {}
    
    _args = None
    
    description = 'Closes the current project.'
    
    def _parse(self, parser):
        return
    
    def _set_previous_state(self, previous_state, args):
        project = Model.Instance().get_curr_project()
    
        if not project:
            raise CommandError("Must open a project first")
    
        previous_state['path'] = project.path
        previous_state['url'] = project.url
    
    
    def _do(self, args):
        try:
            Model.Instance().close_project()
        except ProjectNoneException:
            raise CommandError("Must open a project before")
        return 'The current project has been closed'
    
    def undo(self, state):
        project = Model.Instance().open_project(state['path'], state['url'])
    

class CommandProjDelete(Command):

    _previous_state = {}

    _args = None

    description = 'Closes and delete the current project.'

    def _parse(self, parser):
        return parser

    def _set_previous_state(self, previous_state, args):
        project = Model.Instance().get_curr_project()

        if not project:
            raise CommandError("Must open a project first")

        previous_state['name'] = project.name
        previous_state['path'] = project.path
        previous_state['url'] = project.url

    def _do(self, args):
        Model.Instance().close_project(delete=True)

    def undo(self, state):
        print "This command cannot be undone"


class CommandProjOpen(Command):

    _previous_state = {}

    _args = None

    description = 'Opens a new project and sets it as the current project.'

    def _parse(self, parser):
#        parser.add_argument('name',     type=str,                help='name of the project to open')
        parser.add_argument('path',     type=str, nargs='?',    help='path of the project')
        parser.add_argument('url',      type=str, nargs='?',    help='URL for fetching the project source code')

    def _set_previous_state(self, previous_state, args):
        project = Model.Instance().get_curr_project()

        if project is not None:
            previous_state['path'] = project.path
            previous_state['url'] = project.url
        else:
            previous_state['path'] = None
            previous_state['url'] = None

    def _do(self, args):
        curr_project = Model.Instance().get_curr_project()
        if curr_project is not None:
            raise CommandError("Before opening project '%s' close current project '%s'" % (args.path, curr_project.path))

        try:
#            print "path: %s, url: %s" % (args.path, args.url)
            Model.Instance().open_project(args.path, args.url)
        except Exception, e:
            raise CommandError("Error opening project '%s'. Details: %s" % (args.path, e))

    def undo(self, state):
        if state['path']:
            project = Model.Instance().open_project(state['path'], state['url'])
        else:
            Model.Instance().close_project()
