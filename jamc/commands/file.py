from jamc.commands import Command
from jamc.errors import CommandError
import logging
import sys
import os
import subprocess
import logging

log = logging.getLogger(__name__)

class CommandFileOpen(Command):

    description = 'Open the file with the system editor (only works in Linux at the moment)'

    EXT_TO_CMD = {
        "xml": "uppaal",
    }

    def _parse(self, parser):
        parser.add_argument("filename", type=str, help="the file to be opened")

    def _set_previous_state(self, previous_state, args):
        pass

    def _do(self, args):

        if not os.path.exists(args.filename):
            raise CommandError("The file '%s' does not exist" % args.filename)

        filename, ext = os.path.splitext(args.filename)
        log.debug("Open file: (%s,%s)" % (filename, ext))
 
        cmd = self.EXT_TO_CMD.get(ext.strip("."), "xdg-open")

        try:
            subprocess.Popen([cmd, args.filename])
        except Exception, e:
            raise CommandError("Cannot read file '%s'. Details: %s" % (args.filename, e))

    def undo(self, state):
        pass


