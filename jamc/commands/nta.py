from jamc.commands import Command
from jamc.models import Model
from java2ta.ta.models import NTA, TA
from java2ta.ta.views import Uppaal
from java2ta.translator.models import FreshNames
from jamc.errors import CommandError

import re
import logging
import subprocess
import tempfile
import os

log = logging.getLogger()


class CommandNTASave(Command):

    def _parse(self, parser):
        parser.add_argument('nta_name', type=str, help="the name of the NTA to be saved")
        parser.add_argument('path', type=str, help="the path where the file should be saved")

    def _do(self, args):

        try:
            u = Model.Instance().get_nta(args.nta_name)
            assert isinstance(u, Uppaal)
            nta = u.nta
        except ValueError, e:
            raise CommandError("Error looking for NTA '%s': %s" % (args.nta_name, e))

        try:
            uppaal = Uppaal(nta)
            uppaal.save(args.path)
        except Exception, e:
            raise CommandError("Error saving the NTA on the path: %s. Details: %s" % (args.path, e))


class CommandNTAModelCheck(Command):
    """
    This command allows to model check a timed automaton.
    Required arguments:
    - the name of the timed automaton
    - the name of the formula to be checked
    """

    def _parse(self, parser):
        parser.add_argument('nta_name', type=str, help='the name of the NTA you want to model check')
        parser.add_argument('formula_name', type=str, help='the name of the formula to use as specification')
        parser.add_argument('ta_names', type=str, help='list of TA names linked to the specification')
        parser.add_argument('--debug', '-d', action="store_true", default=False, help="enable debug mode (don't delete temporary files, for future analysis)")

    def _do(self,args):

        try:
            u = Model.Instance().get_nta(args.nta_name)
            nta = u.nta
        except ValueError, e:
            raise CommandError("Error looking for NTA '%s': %s" % (args.nta_name, e))

        formula = Model.Instance().get_formula(args.formula_name)
    
        if formula is None:
            raise CommandError("Formula not found")

        ta_names = args.ta_names.strip("()").split(",")
#        processes = map(lambda proc: nta.get_ta(proc), processes)
        formula_env = { proc_name : nta.get_ta(ta_name) for (proc_name,ta_name) in zip(formula.process_names, ta_names) }

        temp_model = None
        temp_spec = None
        try:
            # 1. save uppaal model of TA in temporary file 
            model_name_prefix = "jamc_model_%s_" % args.nta_name
            with tempfile.NamedTemporaryFile(prefix=model_name_prefix, suffix=".xml", delete=False) as temp_model:
                uppaal = Uppaal(nta, ignore_positions=True)
                log.debug("Saving model to temporary file: %s" % temp_model.name)
                uppaal.save(temp_model.name)

            # 2. save uppaal specification to temporary file
            spec_name_prefix = "jamc_spec_%s_" % args.formula_name
            with tempfile.NamedTemporaryFile(prefix=spec_name_prefix, suffix=".tctl", delete=False) as temp_spec:
                uppaal_spec = formula.to_uppaal(formula_env)
                #if args.debug:
                #    print "Specification: %s" % uppaal_spec
                log.debug("Saving formula to temporary file: %s" % (temp_spec.name))
                temp_spec.write(uppaal_spec)        

            # 3. run verifyta passing the two temporary files as argument
            # verifyta: 
            # -s : suppress progress indicator
            # -t 0 : generate some trace
            # -X prefix : save the trace file with given prefix (and extension .xtr)
            trace_prefix = "%s_%s_" % (args.nta_name, args.formula_name)
            command = ["verifyta", "-s", "-t", "0", "-X", trace_prefix, temp_model.name, temp_spec.name]

            if args.debug:
                print "Command: %s" % (" ".join(command),)

            outcome = subprocess.check_output(command)

            # 4. parse the outcome and show the output to the user
            # credits: https://stackoverflow.com/a/39416125
            log.debug("Verification outcome: %s" % outcome)
            #remove_control_characters = lambda text: re.sub(r'\x1b[^m ]*m', '', text)
            #outcome = remove_control_characters(outcome)

            if args.debug:
                print outcome

            #if os.path.exists(trace_path):
            m = re.search("XMLTrace outputted to: (.*\.xml)", outcome)
            if m is not None: #"Formula is NOT satisfied" in outcome:
                trace_path = m.group(1) 
                assert trace_path.startswith(trace_prefix)
                assert os.path.exists(trace_path)

                try:
                    (steps, is_looping) = Model.Instance().parse_uppaal_trace(nta, trace_path)
                except Exception as e:
                    raise CommandError("Error while parsing Uppaal trace. Details: %s" % e)

                prev = {}
                for i,state in enumerate(steps):
                    edges = [] # collects here the edge taken by every moving process
                    state_output = [] # collects here the state of every process
                    for proc, location in sorted(state.iteritems()):
                        pred = location.predicate
                        pc = location.pc
                        state_output.append("  |- %s : %s%s" % (proc, pred, pc))
                        proc_edge = []
                        if proc in prev:
                            proc_edge = prev[proc].select_outgoing(location)
                            if len(proc_edge) > 0:
                                assert len(proc_edge) == 1
                                edges.append("%s <%s>" % (proc, proc_edge[0]))
                        prev[proc] = location

                    # at least one process has moved
                    assert i == 0 or len(edges) > 0  

                    # first print the edges taken by each moving process; 
                    # next print the state of every process
                    if i == 0:
                        print "- %s" % i
                    else:
                        print "  |"
                        print "- %s [%s]" % (i, ", ".join(edges))

                    print "  |"
                    print "\n".join(state_output)

    
                if is_looping:
                    print "  |"
                    print "  |-- (loop)"
    
        except:    
            # PS note that log.exception will show the exception stacktrace even though we
            # don't pass a reference to the exception object
            log.exception("Error while saving uppaal model and specification to temporary files and model checking")
        finally:
            if not args.debug:
                if temp_model and os.path.exists(temp_model.name):
                    os.unlink(temp_model.name)
                if temp_spec and os.path.exists(temp_spec.name):
                    os.unlink(temp_spec.name)

class CommandNTADefine(Command):

    description = 'Define a network of timed automata'

    def _parse(self, parser):
        parser.add_argument('name', type=str, help="a unique identifier for this NTA")
        parser.add_argument('process', type=str, nargs="*", help='a list of name:template')


    def _do(self, args):

        nta = NTA()
        for spec in args.process:
            proc_name, ta_template_name = spec.split(":")
            ta_template_info = Model.Instance().get_ta_info(ta_template_name)
            if not ta_template_info:
                raise CommandError("Cannot find TA template '%s'" % ta_template_name)

            (_,_,_,ta_template) = ta_template_info

            ta = TA(proc_name, ta_template)
            nta.add_ta(ta)

        Model.Instance().set_nta(args.name, nta)
        

class CommandNTAPrintTrace(Command):

    def _parse(self, parser):
        parser.add_argument('trace', type=str, help="path of the trace to be printed")
        parser.add_argument('nta', type=str, help="the name of the NTA generting the trace")

    def _do(self, args):

        if not os.path.exists(args.trace):
            raise CommandError("No trace to parse")

        u = Model.Instance().get_nta(args.nta)
        assert isinstance(u, Uppaal)
        nta = u.nta


       
