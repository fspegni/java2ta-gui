from deprecated import deprecated
import shlex
import pkg_resources
import os
import re
import copy
import subprocess
import time
import shutil

from contracts import contract, check

from java2ta.ir.models import *
from java2ta.translator.shortcuts import *
from java2ta.abstraction.shortcuts import *
from java2ta.abstraction.models import *
from java2ta.ta.views import Uppaal, UppaalTraceParser

from config import *
from errors import CommandError, ProjectNoneError
from singleton import Singleton
from request import Request

import git
from git import Repo

import logging

log = logging.getLogger("main")

@Singleton
class Model:

    # istanza della classe java2ta.ir.models.Project che consiste nel progetto corrente
    __curr_project = None


    # dizionario di tutti i metodi importati nel corso dell'esecuzione del programma (possono appartenere a progetti diversi),
    # le chiavi sono date da nomi identificativi (questo nome puo' essere diverso dal nome che troviamo nella firma del metodo)
    # e i valori da istanze della classe java2ta.ir.models.Method
    __methods = {}


    # dizionario le cui chiavi sono i nomi e i valori sono attributi (ulteriori dizionari)
    #    un attributo e' un dizionario le cui chiavi sono i nomi e i valori tuple del tipo (vars, preds, domain)
    #        vars e' un dizionario di variabili le cui chiavi sono i nomi e i valori sono i tuple del tipo (domain, is_local)
    #            (domain fara' riferimento ad una chiave del dizionario DATATYPES)
    #        preds e' una lista di predicati, ogni elemento di tale lista e' una tupla del tipo (operator, operand1, operand2)
    #        domain e' una tupla del tipo (var_list, domain, is_local), tale tupla servira' a java2ta per creare gli FSA
    __statespaces = {}


    # dizionario degli automi a stati finiti generati, le chiavi sono i nomi di questi FSA (che sono anche i nomi dei file dove vengono salvati),
    # i valori sono tuple del tipo (method_name, ss_name)
    __ta = {}

    __formulas={}

    __ntas={}

    # apre un progetto e lo imposta come progetto corrente, ogni comando eseguito successivamente fara' riferimento a tale progetto
    def open_project(self, project_path, url=None):

        project_path = os.path.abspath(project_path)

        if url:
            # if the specified path exists, check that it contains the
            # git project with the remote url specified; otherwise,
            # clone locally the project at the git remote url
            try:
                if os.path.exists(project_path):
                    log.debug("Project path exists: %s. Check remote url: %s" % (project_path, url))
                    # this verifies that the path actually contains a 
                    # git repository
                    r = Repo(project_path)
                    remote_urls = map(lambda rem: rem.url, r.remotes)

                    if url not in remote_urls:
                        raise CommandError("The specified directory contains a git project with remote urls: %s. Expected: %s" % (",".join(remote_urls), url))
                else:
                    # clone the project from the passed remote url
                    Repo.clone_from(url, project_path)
            except git.exc.GitCommandError, e:
                log.debug("Git error: %e")
                raise CommandError("Error fetching the project URL")

        project_name = os.path.basename(project_path)

        project_path = "file://%s" % project_path
        project = Project(project_name, path=project_path, url=url, api_url=DEFAULT_API_URL)
        project.open(sync=True)
        if not project.is_open():
            raise CommandError("Cannot open project '%s' in path %s (url: %s)" % (project_name, project_path, url))
        self.__curr_project = project


    # chiude il progetto corrente impostandolo il valore dell'attributo a None
    def close_project(self, delete=False, clean=False):
        """
        Close the current project and, if specified, delete it from the
        filesystem.
        """

        project_path = None

        if not self.__curr_project:
            raise ProjectNoneException

        if clean:
            self.__curr_project.clean()

        project_path = self.__curr_project.path

        self.__curr_project = None

        if delete and project_path is not None:
            try:
                shutil.rmtree(project_path)
            except IOError, e:
                print "Error deleting project '%s'. Details: %s" % (self.__curr_project.name, e)


    def get_curr_project(self):
        """
        return the instance of current project
        """
        return self.__curr_project


    def get_methods(self):
        """
        return the current dictionary of methods
        """
        return self.__methods


    @contract(name="string", file_name="string", class_name="string")
    def add_method(self, name, file_name, class_name, method_signature):
        """
        create a new method and set it as the current one
        """
##        if not Request.Instance().is_class_in_project(file_name, class_name, self.get_curr_project()):
##            raise ValueError('The class "' + class_name + '" associated to a file named "' + file_name + '" doesn\'t exist in the current project')
##        if not Request.Instance().is_method_in_class(method_signature, self.get_curr_project(), file_name, class_name):
##            raise ValueError('The method "' + name + '" doesn\'t exist in the class "' + class_name + '"')
#        existing_name = self.__method_exists(file_name, class_name, method_signature)
#        if existing_name:
#            raise ValueError('This method has already been imported, with the name "' + existing_name + '"')

        user_method = self.__curr_project.get_method(class_name, file_name, method_signature)

        log.debug("Found user method: %s" % user_method)

        if not user_method: 
             raise ValueError("Error looking for user method: class name = %s, file name = %s, method signature = %s" % (class_name, file_name, method_signature))
           

        if not user_method.exists:
            raise ValueError("Cannot find user method: class name = %s, file name = %s, method signature = %s" % (class_name, file_name, method_signature))

        if not user_method.has_instructions:
            raise ValueError("The user method has been found but has no instructions: class name = %s, file name = %s, method signature = %s. Perhaps it contains a syntax error?" % (class_name, file_name, method_signature))

        self.__methods[name] = user_method


    def get_method(self, name):
        if name not in self.__methods:
            raise ValueError("No method with given name: %s" % name)

        return self.__methods[name]
            

    def rename_method(self, old_name, new_name):
        """
        rename the method with given name in the current method 
        dictionary
        """
        if old_name == new_name:
            raise CommandError('Seriously?')
        if old_name not in self.__methods:
            raise CommandError('The method "' + old_name + '" doesn\'t exist')
        if new_name in self.__methods:
            raise CommandError('There is already another method named "' + new_name + '"')
        self.__methods[new_name] = self.__methods.pop(old_name)


    # elimina un metodo e gli spazi degli stati associati ad esso, se tale metodo era anche il metodo corrente viene eliminato il riferimento
    def delete_method(self, name):
        if name not in self.__methods:
            raise CommandError('The method "' + name + '" doesn\'t exist')
        del self.__methods[name]


    # aggiunge al dizionario dei metodi una copia dell'istanza passata come parametro,
    # non vengono effettuati controlli perche' essendo un'operazione di undo non puo' generare eccezioni
    def undo_delete_method(self, name, method):
        self.__methods[name] = copy.copy(method)


    # restituisce una descrizione del metodo passato come parametro
    def method_to_string(self, method):
        desc =    'Project: '                + method.klass.project.name
        desc += '\nPath: '                + method.klass.project.path
        desc += '\nFile: '                + method.klass.path
        desc += '\nClass: '                + method.klass.name
        desc += '\nMethod signature: '    + method.name
        return desc


    # restituisce il dizionario degli spazi degli stati, se e' vuoto viene generata un'eccezione
    def get_statespaces(self):
        if not self.__statespaces:
            raise CommandError('You haven\'t defined any state spaces yet')
        return self.__statespaces


    # restituisce (se esiste nel dizionario __statespaces) l'istanza dello spazio degli stati con il nome uguale al parametro 'name'
    @contract(name="string", returns="dict")
    def get_statespace_attributes(self, name):
        if name not in self.__statespaces:
            raise CommandError('The statespace "' + name + '" doesn\'t exist')
        return self.__statespaces[name]


    @contract(ss_name="string", returns="is_state_space")
    def get_statespace(self, ss_name):
        statespace_attributes = self.get_statespace_attributes(ss_name)

        domains = []
        for attr_name, (_, _, domain_tuple, _) in statespace_attributes.items():
            domains.append(domain_tuple)

        ss = StateSpace()

        for (var_names, domain, status) in domains:
            check('list(string)|string', var_names)
            check('is_domain', domain)
            check('is_attribute_status', status)
            
            abs_att = AbstractAttribute(var_names, domain, status)
            ss.add_attribute(abs_att)
    
        return ss
    
    @contract(name="string",formula="*", signature="dict(string:string)")
    def add_formula(self, name, formula, signature):
        if name in self.__formulas:
            raise CommandError('The name "' + name + '" is already associated to another formula')

        ss_env = {}
        for proc_name,ss_name in signature.iteritems():
            ss_env[proc_name] = self.get_statespace(ss_name)
        fp=FormulaParser(ss_env)           

        self.__formulas[name] = fp.parse(formula)
        
    def delete_formula(self, name):
        if name not in self.__formulas:
            raise CommandError('The formula "' + name + '" doesn\'t exist')
        del self.__formulas[name]
        
    def get_formulas(self):
        return self.__formulas

    @contract(name="string")
    def get_formula(self, name):
        """
        Return a pair with a PathFormula and a list of strings; the latter is the "signature" of the
        formula, i.e. it contains all the process names used in the formula
        """
#        log.debug("All formulas: %s" % self.__formulas)
        formula = self.__formulas.get(name, None)

        if not formula:
            raise ValueError("No formula with the given name: %s" % name)
    
        return formula

    # crea un nuovo spazio degli stati e lo associa al metodo corrente
    def add_statespace(self, name):
#        if name in self.__statespaces:
#            raise CommandError('The name "' + name + '" is already associated to another state space')
        self.__statespaces[name] = {}


    # rinomina lo spazio degli stati con il nome uguale al parametro 'old_name' con il nome dato dal parametro 'new_name'
    def rename_statespace(self, old_name, new_name):
        if old_name == new_name:
            raise CommandError('You are renaming the state space passing the same name. This is probably not what you want.')
        if old_name not in self.__statespaces:
            raise CommandError('The state space "' + old_name + '" doesn\'t exist')
        if new_name in self.__statespaces:
            raise CommandError('There is already another state space named "' + new_name + '"')
        self.__statespaces[new_name] = self.__statespaces.pop(old_name)


    # elimina uno spazio degli stati
    def delete_statespace(self, name):
        if name not in self.__statespaces:
            raise CommandError('The state space "' + name + '" doesn\'t exist')
        del self.__statespaces[name]


    # aggiunge al dizionario degli spazi degli stati una copia dell'istanza passata come parametro,
    # non vengono effettuati controlli perche' essendo un'operazione di undo non puo' generare eccezioni
    def undo_delete_statespace(self, name, statespace):
        self.__statespaces[name] = copy.copy(statespace)


    def statespace_to_string(self, statespace):
        """
        restituisce una descrizione dello spazio degli stati passato
        come parametro
        """
        desc = '\n-----------------------\n'
        for attr_name, (var_list, pred_list, domain_tuple, is_local) in statespace.items():
            desc += 'Attribute: ' + attr_name
            desc += '\nVar list: '

            print "items: %s" % var_list.items()
            for var, dom in var_list.items():
                desc += var + ' (' + dom + '); '
            desc += '\nPred list: '
            for pred in pred_list:
                desc += str(pred) + "; "
            desc += '\nIs local: %s' % is_local
            desc += '\n-----------------\n'
        return desc


    def get_attribute_by_name(self, ss_name, attr_name):
        """
        restituisce (se esiste) l'istanza dell'attributo con il nome 
        specificato dal parametro attr_name
        """
        attributes = self.get_statespace_attributes(ss_name)
        if attr_name not in attributes:
            raise CommandError('The attribute "' + attr_name + '" doesn\'t exist in the state space "' + ss_name + '"')
        return attributes[attr_name]

    #@contract(ss_name="string", attr_name="string", vars_string_list="string", preds_string_list="string", status="is_attribute_status")
    def add_attribute(self, ss_name, attr_name, vars_string_list, preds_string_list, status):
        """
        crea un nuovo attributo e lo associa ad uno spazio degli stati
        """
        attributes = self.get_statespace_attributes(ss_name)
        var_list = self.__parse_var_list(vars_string_list)
        pred_list = self.__parse_pred_list(preds_string_list)
        domain_tuple = self.__get_attribute_domain(var_list, pred_list, status)
        attributes[attr_name] = (var_list, pred_list, domain_tuple, status)

        log.debug("New attribute: %s : %s" % (attr_name, attributes))

    def rename_attribute(self, ss_name, old_name, new_name):
        """
        rinomina gli attributi nello spazio degli stati specificato da
        ss_name
        """
        attributes = self.get_statespace_attributes(ss_name)
        if old_name == new_name:
            raise CommandError('Seriously?')
        if old_name not in attributes:
            raise CommandError('The attribute "' + old_name + '" doesn\'t exist in the state space "' + ss_name + '"')
        if new_name in attributes:
            raise CommandError('There is already another attribute named "' + new_name + '" in the state space "' + ss_name + '"')
        attributes[new_name] = attributes.pop(old_name)


    # elimina un attributo da uno spazio degli stati
    def delete_attribute(self, ss_name, attr_name):
        attributes = self.get_statespace_attributes(ss_name)
        if attr_name not in attributes:
            raise CommandError('The attribute "' + attr_name + '" doesn\'t exist in the state space "' + ss_name + '"')
        del attributes[attr_name]


    # aggiunge al dizionario degli attributi di uno spazio degli stati una copia dell'istanza passata come parametro,
    # non vengono effettuati controlli perche' essendo un'operazione di undo non puo' generare eccezioni
    def undo_delete_attribute(self, ss_name, attr_name, attribute):
        attributes = self.get_statespace_attributes(ss_name)
        attributes[attr_name] = copy.copy(attribute)

    def remove_ta(self, ta_name):
        del self.__ta[ta_name]

    def get_tas(self):
        return self.__ta

    @contract(name="string", returns="is_ta_template")
    def get_ta(self, name):
        ta = None

        try:
            (method_name, ss_name, ss, ta) = self.get_ta_info(name)
        except Exception, e:
            raise ValueError("Error retrieving timed automaton '%s': %s" % (name, e))

        return ta

    def get_ta_info(self, ta_name):
        ta_info = self.__ta.get(ta_name, None)

        if ta_info is None:
            raise ValueError("Timed automaton '%s' does not exist" % ta_name)

        return ta_info
    
    def set_ta_info(self, ta_name, ta_info):
        self.__ta[ta_name] = ta_info

    def get_all_ta(self, verbose = False):
        """
        return the dictionary of timed automata
        """
        return self.__ta


    # restituisce una stringa contenente il progetto e il metodo corrente, che verranno visualizzati ad ogni richiesta di input, 
    # con valore eventualmente 'none' (non vengono generate eccezioni)
    def get_current_state(self):
        curr_project = self.get_curr_project()
    
        state = "<no_project>::<no_method>"

        state_project = "<no_project>"
        state_methods = "<no_method>"
        state_tas = "<no_ta>"

        if curr_project is not None:
            state_project = curr_project.name

        num_methods = len(self.get_methods())
        if num_methods > 0:
            plural = "s" if num_methods > 1 else ""
            state_methods = "%s method%s" % (num_methods, plural)
    
        num_tas = len(self.get_tas())
        if num_tas > 0:
            plural = "s" if num_tas > 1 else ""
            state_tas = "%s TA%s" % (num_tas, plural)

        state = "%s::%s::%s" % (state_project, state_methods, state_tas)

        return state


    # controlla se e' stato gia' importato un metodo con i parametri passati
    @contract(file_name="string", class_name="string", method_signature="tuple")
    def __method_exists(self, file_name, class_name, method_signature):
        for name, method in self.__methods.items():
            if(self.__curr_project.name    == method.klass.project.name    and
                'file://' + file_name    == method.klass.path            and
                class_name                == method.klass.name            and
                method_signature        == method.name):
                return name
        return False


    # prende in input una stringa del tipo "variable1:domain1,variable2:domain2,[...]variableN:domainN"
    # e restituisce un dizionario del tipo {variable1: domain1, variable2: domain2, [...], variableN: domainN}
    def __parse_var_list(self, var_string):
        var_string_list = var_string.split(',')
        var_dict = {}
        for var in var_string_list:
            res = re.match(r'[ ]*([a-zA-Z0-9_]+)[ ]*([:|])[ ]*([a-zA-Z0-9_]+)[ ]*\Z', var)
            if not res:
                raise CommandError('The list of variables doesn\'t match the correct format')

            var_name = res.group(1)
            if var_name in var_dict:
                raise CommandError('The variable "' + var_name + '" has been declared more than once')
            
#            is_local = True if res.group(2) == '|' else False
            var_domain = res.group(3)

#            var_dict[var_name] = (var_domain, is_local)
            var_dict[var_name] = var_domain

        return var_dict

    @contract(pred_string="string", returns="list(is_predicate)")
    def __parse_pred_list(self, pred_string):

        pp = PredicateParser()
        predicates = []
        pp = PredicateParser()
        for text in pred_string.split(","):
            pred = pp.parse(text)
            predicates.append(pred)

        return predicates


    
    # restituisce una tupla con una struttura del tipo ([var_list, domain, is_local])
    #    - var_list e' una lista di variabili che contiene solo i nomi, non i domini [var1, var2, ...]
    #     - domain e' un'istanza di un'opportuna classe che estende la classe Domain,
    #        e' il prodotto tra i domini delle variabili opportunamente partizionato in funzione dei predicati
    #    - status e` una enumerazione
    # la lista dei domini passati a java2ta per la creazione degli FSA consistera' in tuple di questo tipo, una per ogni attributo
    #@contract(var_repr="dict(string:string)", predicates="list(is_predicate)", status="is_attribute_status", returns="tuple(list(string)|string,is_domain,is_attribute_status)")
    def __get_attribute_domain(self, var_repr, predicates, status):
        """
        TODO rivedere completamente questo metodo per consentire di
        specificare direttamente un dominio, anziche` costruirlo da linea di comando ...
        """

        variables = []
        datatypes = []
        for var,spec in var_repr.iteritems():
            dt, default_dom = DATATYPES.get(spec, (None,None))
    
            if not dt:
                raise ValueError("Cannot determine variable type. Variable: %s:%s" % (var, spec))
 
            check("is_data_type", dt)
            check("is_domain", default_dom)
   
#            variables.append((var, dt))
            variables.append(var)
            datatypes.append(dt)
    
            if not variables:
                raise ValueError("Cannot parse variable from text: '%s'" % vars_repr)
        
        log.debug("Variables: %s" % variables)
        domain = None
        if len(datatypes) == 1:
            dt = datatypes[0]
            domain = Domain(dt, predicates)
        else:
            domain = CompareVariables(datatypes, predicates)

        return (variables, domain, status)


    @contract(value="string", returns="bool")
    def _is_literal(self, value):
        """
        Take as input the string contained in a predicate; depending on
        it's content it returns True if such value is a literal, and
        it returns False otherwise
        """
        # check it's a bollean constant
        if value in [ "true", "false", "True", "False" ]:
            return True

        try:
            # check it's a number
            float(value)
            return True

        except ValueError:
            # check whether value is a quoted string
            return not re.match(r'(^"[^"]*"$)|(^\'[^\']*\'$)', value)


    # costruisce la legenda di un FSA
    @contract(state_space="is_state_space")
    def build_legend(self, state_space):
        legend = []
#        print("Build legend. Attributes: %s" % state_space.attributes)
        for (idx,attr) in enumerate(state_space.attributes):
            #check("is_attribute_predicate", attr)
            pred_labels = []
            for idx_attr,pred in enumerate(attr.values):    
            #    check("is_predicate", pred)
                ctx = {}
                for p_var,a_var in zip(pred.var_names, attr.variables):
                    key = p_var.strip("{}")
                    ctx[key] = a_var
                pred_labels.append("%s : { %s }" % (idx_attr, pred.label(**ctx)))
            desc = ", ".join(pred_labels)
#            print("Legend: (%s,%s) : %s" % (idx,attr,desc)) 
            legend.append((str(idx),desc))
        return legend

    @contract(name="string", nta="is_nta")
    def set_nta(self, name, nta):

        u = Uppaal(nta)
        self.__ntas[name] = u


    @contract(name="string", returns="is_uppaal")
    def get_nta(self, name):
        found = self.__ntas.get(name, None)

        if not found:
            raise ValueError("Cannot find NTA with name '%s'" % name)

        assert isinstance(found, Uppaal)

        return found


    @contract(name="string", path="string", ignore_positions="bool")
    def save_nta(self, name, path, ignore_positions=False):
        found = self.get_nta(name)

        assert isinstance(found, Uppaal)
        found.ignore_positions = ignore_positions
        found.save(path)


    @contract(old_path="string", new_path="string")            
    def _copy_file(self, old_path, new_path):
        assert not os.path.exists(new_path)
        assert os.path.exists(old_path)
        with open(new_path, "w+") as new_file:
            with open(old_path, "r") as old_file:
                new_file.write(old_file.read())


    @contract(path="string", returns="tuple(list, bool)")
    def parse_uppaal_trace(self, nta, path):
    
        utp = UppaalTraceParser(nta, path)
        return utp.parse()
