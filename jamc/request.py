from errors import CommandError
from singleton import Singleton
from java2ta.ir.client import APIError

@Singleton
class Request:

	def get_classes_from_project(self, project):
		classes = []
		files = project.get_files()
		for file in files:
			data = project.get_file(file)
			for class_data in data:
				classes.append((file, class_data['name']))
		if not classes:
			raise CommandError('The project "' + project.name + '" is empty')
		return classes


	def get_methods_from_class_name(self, project, file_name, class_name):
		methods = []
		try:
			class_data = project.get_class(class_name, file_name)
		except APIError:
			raise CommandError('The file "' + file_name + '" doesn\'t exist in the current project')
		if class_data is None:
			raise CommandError('The class "' + class_name + '" doesn\'t exist in the file "' + file_name + '"')
		for method_data in class_data['methods']:
			methods.append(method_data['name'])
		if not methods:
			raise CommandError('The class "' + class_name + '" is empty')
		return methods


##	def is_class_in_project(self, file_name, class_name, project):
##		if file_name in project.get_files():
##			data = project.get_file(file_name)
##			for class_data in data:
##				if class_data['name'] == class_name:
##					return True
##		return False
##

##	def is_method_in_class(self, method_signature, project, file_name, class_name):
##		class_data = project.get_class(class_name, file_name)
##		for method_data in class_data['methods']:
##			if method_data['name'] == method_signature:
##				return True
##		return False
