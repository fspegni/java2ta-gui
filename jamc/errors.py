# in questo file sono definite eccezioni personalizzate, in particolare la piu' generale CommandError (dalla quale estendono le altre),
# che puo' essere generata durante l'esecuzione di un comando e permette di definire un messaggio d'errore specifico caso per caso

class CommandError(Exception):
	pass

class ProjectNoneError(CommandError):

    def __init__(self,*args,**kwargs):
        message = 'You have to open a project first'
       
        super(ProjectNoneError, self).__init__(message, *args, **kwargs)
