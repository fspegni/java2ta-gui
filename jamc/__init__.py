import logging.config
import os
import re
import sys 
import argparse

from jamc.ui import UI
from jamc.models import Model
from prompt_toolkit import prompt
from prompt_toolkit.key_binding.manager import KeyBindingManager
from prompt_toolkit.keys import Keys
from prompt_toolkit.styles import style_from_dict
from prompt_toolkit.token import Token
from prompt_toolkit.history import InMemoryHistory
from prompt_toolkit.auto_suggest import AutoSuggestFromHistory
from prompt_toolkit.completion import Completion
from prompt_toolkit.contrib.completers import WordCompleter

from jamc.commands.proj import ( CommandProjOpen, CommandProjClose, 
                            CommandProjDelete, CommandProjClean, 
                            CommandProjClasses )
from jamc.commands.method import ( CommandMethodList, CommandMethodAdd, 
                            CommandMethodDel, CommandMethodRen,
                            CommandMethodInterpret, CommandMethodInterpretation )
from jamc.commands.ss import ( CommandSsList, CommandSsCreate, CommandSsShow, 
                            CommandSsRen, CommandSsDel, 
                            CommandSsAttrCreate, CommandSsAttrRen, 
                            CommandSsAttrDel, CommandSsAddTimestamp )
from jamc.commands.ta import ( CommandTAMake, CommandTAGraph,  
                            CommandTAList, CommandTAInfo, CommandTAOptimize,CommandTAInterpret, 
                            CommandTAConstraint, CommandTASet )
from jamc.commands.nta import CommandNTADefine, CommandNTAModelCheck, CommandNTASave
from jamc.commands.log import CommandLog
from jamc.commands.file import CommandFileOpen
from jamc.commands.time import CommandTimeNowMethod, CommandCheckTimestamps, CommandTimeInfo
from jamc.errors import CommandError
from jamc.commands.formulas import CommandFormulaAdd, CommandFormulaDel, CommandFormulaInfo

log = logging.getLogger("main")

manager = KeyBindingManager.for_prompt()
@manager.registry.add_binding(Keys.ControlH)
def _(event):
    def print_hello():
        print "hello world"
    event.cli.run_in_terminal(print_hello)

class CommandCompleter(WordCompleter):

    FILENAME_COMMANDS = [ "load", "store", "proj_open", "file_open" ]

    def __init__(self, ui, *args, **kwargs):
        self.commands = ui.get_command_names()

        # option WORD=True helps in handling file paths
        super(CommandCompleter, self).__init__(self.commands, WORD=True)

    def get_filenames(self, arg):

        base_path = os.path.abspath(arg)

        if not os.path.exists(base_path):
            base_path = os.path.dirname(base_path)
            arg = os.path.dirname(arg)

        filenames = []

        try:
            for f in sorted(os.listdir(base_path)):
                curr = f
                if arg:
                    curr = os.path.join(arg, f)

                filenames.append(curr)

        except Exception:
            pass

        return filenames

    def get_completions(self, document, *args, **kwargs):

        normal_text = re.sub("\s+", " ", document.text.strip())
        tokens = normal_text.split(" ")

#        log.debug("tokens: %s" % tokens)

        if len(tokens) <= 1:
            words = self.commands
        else:
            # 2 or more tokens
            curr_command = tokens[0]

            if curr_command in CommandCompleter.FILENAME_COMMANDS:
                filenames = self.get_filenames(tokens[1])
#                log.debug("word before cursor (%s): %s" % (self.WORD, document.get_word_before_cursor(WORD=self.WORD)))
#                log.debug("file names: %s - > %s" % (tokens[1], filenames))
                # suggest to complete with one of the files
                words = filenames
            else:
                # no suggestion for completion
                words = []

        self.words = words

        return super(CommandCompleter, self).get_completions(document, *args, **kwargs)


def get_bottom_toolbar_tokens(cli):
    """
    TODO if we pass the argument refresh_interval = X, this function
    can be called every X seconds and update the user on the status of
    long running commands
    """
    curr_state = Model.Instance().get_current_state()
    return [(Token.Toolbar, "%s" % curr_state) ]

style = style_from_dict({
    Token.Toolbar : "#ffffff bg:#333333",
})

def get_arg_parser():

    parser = argparse.ArgumentParser(description="Tool for Java code abstraction. Input: Java code. Output: finite state (timed/untimed) automaton")
    parser.add_argument("script", type=str, nargs="?", help="the path of a script to be executed")
    parser.add_argument("--trace", "-t", action="store_true", default=False, help="if specified, print a stacktrace when an exception occurs")
    parser.add_argument("--wait", "-w", action="store_true", default=False, help="if specified together with a script file, the prompt wait for new commands after the script is processed")
    parser.add_argument("--log", action="store_true", help="enable the use of a debug log file")
    parser.add_argument("--log-file", nargs="?", type=str, help="the path of the debug log file")

    return parser

def create_ui(trace=False):

    ui = UI(trace=trace)
    ui.add_command('proj_open', CommandProjOpen)        
    ui.add_command('proj_close', CommandProjClose)
    ui.add_command('proj_clean', CommandProjClean)
    ui.add_command('proj_delete', CommandProjDelete)
    ui.add_command('proj_classes', CommandProjClasses)        
    ui.add_command('method_list', CommandMethodList)        
    ui.add_command('method_add', CommandMethodAdd)    
    ui.add_command('method_ren', CommandMethodRen)        
    ui.add_command('method_del', CommandMethodDel)        
    ui.add_command('method_interpret', CommandMethodInterpret)
    ui.add_command('method_interpretation', CommandMethodInterpretation)
    ui.add_command('ss_list', CommandSsList)            
    ui.add_command('ss_create', CommandSsCreate)        
    ui.add_command('ss_show', CommandSsShow)            
    ui.add_command('ss_ren', CommandSsRen)            
    ui.add_command('ss_del', CommandSsDel)            
    ui.add_command('ss_attr_create', CommandSsAttrCreate) 
    ui.add_command('ss_attr_ren', CommandSsAttrRen)        
    ui.add_command('ss_attr_del', CommandSsAttrDel)    
    ui.add_command('ss_add_timestamp', CommandSsAddTimestamp)
    ui.add_command('ta_make', CommandTAMake)            
    ui.add_command('ta_graph', CommandTAGraph)            
#    ui.add_command('ta_uppaal', CommandTAUppaal)            
    ui.add_command('ta_list', CommandTAList)            
    ui.add_command('ta_info', CommandTAInfo)
    ui.add_command('ta_optimize', CommandTAOptimize)
    ui.add_command('ta_interpret', CommandTAInterpret)
    ui.add_command('ta_constraint', CommandTAConstraint)    
    ui.add_command('ta_set', CommandTASet)
    ui.add_command('log', CommandLog)
    ui.add_command('file_open', CommandFileOpen)
    ui.add_command('time_now', CommandTimeNowMethod)
    ui.add_command('formula_add', CommandFormulaAdd)
    ui.add_command('formula_del', CommandFormulaDel)
    ui.add_command('formula_info', CommandFormulaInfo)
    ui.add_command('time_check_timestamps', CommandCheckTimestamps)
    ui.add_command('time_info', CommandTimeInfo)
    ui.add_command('nta_define', CommandNTADefine)
    ui.add_command('nta_save', CommandNTASave)
    ui.add_command('nta_mc', CommandNTAModelCheck)

    return ui

# questo metodo fa partire l'interfaccia; viene incapsulato in un 
# comando eseguibile tramite lo script di setup
def run():

    arg_parser = get_arg_parser()
    arguments = arg_parser.parse_args()
    
    history = InMemoryHistory()
    auto_suggest = AutoSuggestFromHistory()

    ui = create_ui(arguments.trace)

    if arguments.log or arguments.log_file is not None:
        log_file = arguments.log_file or ("%s.log" % os.path.basename(sys.argv[0]))
        print "log enabled: file=%s ..." % log_file


        try:
            ui.command("log set_file %s" % log_file)
            ui.command("log on")
        except CommandError, e:
            print "Error setting log: %s" % e
    else:
        # by default, enable logging of WARNING (or higher level) messages
        ui.command("log set_level WARNING")
        ui.command("log on")

    if arguments.script is not None:
        try:
            ui.command('load %s' % arguments.script)
        except CommandError, e:
            print "Error executing command from script: %s" % e

        if arguments.wait is False:
            ui.cmd_quit()


    while True:
        try:
            input = prompt(u'>> ', history = history, auto_suggest = auto_suggest, get_bottom_toolbar_tokens = get_bottom_toolbar_tokens, style = style, completer = CommandCompleter(ui), key_bindings_registry =  manager.registry)
            ui.command(input)
        except EOFError:
#            input = "quit"
            ui.cmd_quit()
        except CommandError, e:
            print "Error: %s" % e
        except KeyboardInterrupt:
            pass


