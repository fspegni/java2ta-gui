from java2ta_gui.commands import Command
import pprint


class TestingCommand(Command):
    description = 'We test we compile we conquer'

    def __init__(self, *args, **kwargs):
        super(TestingCommand, self).__init__(*args, **kwargs)

        self.pp = pp = pprint.PrettyPrinter(indent=4)

    # consente di personalizzare le opzioni del comando, aggiungendone altre oltre a quelle di default
    def _parse(self, parser):
        parser.add_argument('-v', '--verbose', action='store_true', help='shows more detailed information, this option has no effect if the classpath is specified too')
        parser.add_argument('-f', '--value', dest='valore', help='shows more detailed information, this option has no effect if the classpath is specified too')
        self.pp.pprint("Parse")
        self.pp.pprint(parser)

    # contiene le azioni da compiere dal comando, dati gli argomenti specificati
    def _do(self, args):
        self.pp.pprint("Do")
        self.pp.pprint(args)
