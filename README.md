# TODO #

## WORKFLOW DI SVILUPPO ##

Descriviamo il workflow di sviluppo utilizzando `virtualenv` per isolare lo sviluppo di questo progetto da (eventuali) altri progetti python sviluppati nella stessa macchina. Se si usano le distribuzioni Debian o Ubuntu, installare i pacchetti `virtualenv` e `virtualenv-wrapper`. 
Nel caso di altre distribuzioni, fare riferimento al sito del/i mantainer della distribuzione stessa. Dopo aver installato `virtualenvwrapper` modificare il proprio file `~/.bashrc` ed aggiungere le righe seguenti:

    WORKON_HOME=$HOME/.virtualenvs
    source /usr/share/virtualenvwrapper/virtualenvwrapper.sh

Proseguire coi passi seguenti.

* clonare il repository corrente in una cartella (nel seguito assumeremo che sia: $HOME/git/java2ta-gui) e posizionarsi in tale cartella:

        $ cd $HOME/git
        $ git clone <URL REPOSITORY>
        $ cd $HOME/git/java2ta-gui


* creare un ambiente virtuale (solo la prima volta; scegliere <nome> a piacere) che punti alla cartella corrente:

    ``$ mkvirtualenv <nome> -a `pwd` ``
    
* entrare nell'ambiente virtuale (solo le volte successive; usare lo stesso <nome> del passo precedente):

    ``$ workon <nome>``
    
* installare le dipendenze del progetto (solo la prima volta e quando cambia il file requirements.txt):

    ``$ pip install -r requirements.txt``

* installare l'applicazione contenuta nel repository corrente (in modalit� editing):

    ``$ pip install -e .``

* iniziare a lavorare

* per uscire dall'ambiente di sviluppo cos� creato eseguire il seguente comando:

    `` $ deactivate``

## DIPENDENZE

Le dipendenze per lo sviluppo dello script in Python si trovano nel file `requirements.txt` (vedi "Workflow di sviluppo"). Altre dipendenze di sistema da soddisfare sono le seguenti:

* dot (graphviz)
* java 
* mongo (>= 3.0)
* Z3 : https://github.com/Z3Prover/z3 (installare la versione adatta al proprio sistema operativo)
* server-<VERSIONE>.jar : nella cartella `deps`
* `java-xal`: � una applicazione python in un repository bitbucket (v. sotto)

Per far partire `server-<VERSIONE>.jar` seguire queste istruzioni:

    $ cd deps
    $ ./start_server.sh

Dopo il comando, un server sara' attivo sulla porta 9000 della vostra macchina. La responsabilita' di questo server sara' quella di eseguire il parser dei file Java che trova in percorsi specificati, e ritornare una rappresentazione intermedia del file/classe parsato. L'operazione di parsing avviene in modalita' *asincrona* (occorre quindi controllare lo stato del task di parsing, prima di richiedere la rappresentazione intermedia risultante). La rappresentazione intermedia e' ritornata sotto forma di dizionario serializzato in JSON. Per terminare il servizio di parsing premere `CTRL+C`.

Per installare `java-xal` clonare il seguente repository: `https://bitbucket.org/fspegni/java-xal` ed installare l'applicazione python contenuta nella sotto-directory `TA`:

    $ cd ~/git
    $ git clone <REPO_URL>
    $ cd java-xal/TA
    $ pip install -e .

Attenzione: il comando di installazione deve avvenire mentre lavorate con il vostro virtualenv di sviluppo.

## TESTING

Gli Unit Test sono contenuti nella cartella `tests`. Si puo' eseguire uno Unit Test semplicemente eseguendo il codice contenuto nel file Python corrispondente, ad esempio:

    $ python unit_test_proj_open.py

Oppure, qualora si volessero eseguire tutti i test contenuti nella cartella, si puo' avviare lo script `test` nella directory `src`:

    $ ./test

Per poter eseguire i test bisogna prima avviare il server di parsing (come mostrato in alto).

Nella cartella `scripts` sono contenuti alcuni file con dei comandi. Ognuno di questi comandi apre il progetto 'testProject' (contenuto nella cartella `projects`), carica un metodo (diverso per ogni file, ma appartenente alla classe TestClass del file TestFile.java), crea uno spazio degli stati con degli attributi (diversi per ogni file) e crea il file con gli automi.
Per testare la corretta creazione degli automi si possono caricare questi file tramite il comando (nella cartella `src`):

    $ python main.py test_name

Dove test_name e' il nome del file.

## DEFINIRE NUOVI COMANDI ##

Un comando e' una classe che estende `java2ta.commands.Command`. In particolare, esso implementa i seguenti metodi:
* `_do(self, args)`: contiene le azioni da compiere dal comando, dati gli argomenti specificati
* `_parse(self, parser)`: consente di personalizzare le opzioni del comando, aggiungendone altre oltre a quelle di default
* `_set_previous_state(self, previous_state, args)`: salva lo stato prima di eseguire il comando
* `undo(self, state)`: riporta lo stato corrente a quello precedente l'applicazione del comando

NB: il supporto per `_Set_previous_state` e `undo` va rivisto.

Per comodit� supponiamo che tutte le classi che implementino un comando si trovino in un modulo del package `java2ta_gui.commands`. Una volta definita la classe per il nostro comando personalizzato, occorre modificare il file `__init__.py` del package `java2ta_gui` come segue:


    from java2ta_gui.commands.XYZ import MyCommandClass
	...
    def run():
	    ...
        ui.add("command_name", MyCommandClass)
		...
		
Da questo momento, � possibile invocare il tool `jamc` e digitando "command_name" si invocher� il nostro comando personalizzato.

## WORK-IN-PROGRESS

### To Do:

* da finire:
	* fsa (path, , )
	* model undo_make_fsa()		eliminare tutti i file FSA con il nome a prescindere dall'estensione (fsa.\*)
	* model open_fsa()			aprire fsa*.pdf
	* comando fsa_make			verificare che non sovrascriva altri file, perche' in caso di undo il file verrebbe eliminato
	*							output
	*							path parametro opzionale
	* unit test proj_open		path relativi e assoluti
	* unit test add_method		testare metodi contenuti in diversi package
	* unit test fsa_make
	* unit test fsa_open
	* unit test fsa_list		eliminare files dopo creazione
	* nel parsing dei predicati, nel controllo regex verificare che l'operatore sia una chiave del dizionario OPERATORS (adesso gli operatori sono scritti in chiaro)
	* successfully stored in the file...

* refactor
	* togliere import inutili, controllare commenti, eccezioni, rendere il codice piu' leggibile in generale
	* migliorare leggibilita' dell'output nei comandi "list"

* da risolvere al piu' presto:
	* gv.save() ignora il nome del file e quindi il path
	* alcuni automi sembrano sbagliati
	* in quali casi i domini passati alla libreria devono contenere anche quelli delle singole variabili? finire model.__get_statespace_domains()
	* gestire le variabili locali		var:dom => parameter		var|dom => local

* "Piccole" modifiche da valutare:
	* salvare tutti gli fsa in una cartella fissa o l'utente puo' scegliere il percorso?
	* uppaal
	* dare la possibilita' di sovrascrivere i file fsa? complicherebbe l'undo
	* gestire meglio il parsing da linea di comando (invece di dover eliminare gli argomenti per farlo funzionare), magari con due parser?
	* avviare il server all'avvio del programma? subprocess, cleanAll ad ogni apertura di progetto
		``from subprocess import *``
		``Popen('java -jar ../deps/server-0.2.jar -debug', shell=True, stdout=PIPE)``
	* refactor: creare metodo _setup nei comandi? il metodo _do gia' si comporta come una transazione in ogni caso (se fallisce nulla viene cambiato),
		* ma per essere sicuri potremmo separare la parte "non critica" (_setup) che non cambia lo stato, dalla parte "critica" (_do)
	
* "Grandi" modifiche da valutare:
	* gestire l'overload dei metodi? (e di conseguenza cambiare comandi come method_add...)
	* considerare altri tipi di predicati e predicati non binari
