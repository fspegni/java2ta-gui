import sys
sys.path.insert(0, '../src')
from init import *


# casi limite
ui.command('proj_close')							# nessun progetto aperto
ui.assert_error('open a project first')

# setup per caso base
ui.command('proj_open ringLeader')

# caso base e verifica
ui.command('proj_close')							# il progetto viene chiuso
ui.assert_success('project', 'closed')
ui.command('proj_classes')
ui.assert_error('open a project first')

# caso base e verifica
ui.command('proj_close')							# come il caso limite
ui.assert_error('open a project first')

# undo e verifica
ui.command('undo')									# undo proj_close (il primo, tutti gli altri comandi sono falliti)
ui.command('proj_classes')							# il progetto aperto e' ora ringLeader
ui.assert_success('RingLeader.java => RingLeader')

# caso base e verifica
ui.command('proj_close')							# come il primo caso base
ui.assert_success('project', 'closed')
ui.command('proj_classes')
ui.assert_error('open a project first')
