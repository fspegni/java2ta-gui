import sys
sys.path.insert(0, '../src')
from init import *


# setup
ui.command('proj_open ringLeader')
ui.command('method_add handleMsg RingLeader.java RingLeader')
ui.command('method_add getLeader RingLeader.java RingLeader')

# casi limite
ui.command('method_ren handleMsg handleMsg')		# stesso nome
ui.assert_error()
ui.command('method_ren wrongMethod name')			# metodo da rinominare inesistente
ui.assert_error('wrongMethod', 'doesn\'t exist')
ui.command('method_ren handleMsg getLeader')		# nuovo nome gia' associato ad un altro metodo
ui.assert_error('another method', 'getLeader')

# caso base e verifica
ui.command('method_ren handleMsg name1')
ui.assert_success()
ui.command('method_list')
ui.assert_success('name1')
ui.assert_missing_success('handleMsg')
ui.command('method_ren handleMsg name2')			# metodo da rinominare inesistente
ui.assert_error('handleMsg', 'doesn\'t exist')

# caso base e verifica
ui.command('method_ren name1 name2')
ui.assert_success()
ui.command('method_list')
ui.assert_success('name2')
ui.assert_missing_success('handleMsg', 'name1')

# undo e verifica
ui.command('undo')									# undo method_list
ui.command('undo')									# undo method_ren name1 name2
ui.command('method_list')
ui.assert_success('name1')
ui.assert_missing_success('handleMsg', 'name2')

# undo e verifica
ui.command('undo')									# undo method_list
ui.command('undo')									# undo method_list
ui.command('undo')									# undo method_ren handleMsg name1
ui.command('method_list')
ui.assert_success('handleMsg')
ui.assert_missing_success('name1', 'name2')

# caso base e verifica
ui.command('method_ren handleMsg name1')			# come caso base
ui.assert_success()
ui.command('method_list')
ui.assert_success('name1')