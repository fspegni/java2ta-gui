import sys
sys.path.insert(0, '../src')
from init import *


# casi limite
ui.command('method_del wrongMethod')				# metodo da eliminare inesistente
ui.assert_error('wrongMethod', 'doesn\'t exist')

# setup per caso base
ui.command('proj_open ringLeader')
ui.command('method_add handleMsg RingLeader.java RingLeader')
ui.command('method_add getLeader RingLeader.java RingLeader')

# caso base e verifica
ui.command('method_del handleMsg')
ui.assert_success()
ui.command('method_list')
ui.assert_missing_success('handleMsg')

# caso base e verifica
ui.command('method_del getLeader')
ui.assert_success()
ui.command('method_list')
ui.assert_error('defined', 'any', 'method')

# undo e verifica
ui.command('undo')									# undo method_del getLeader
ui.command('method_list')
ui.assert_success('getLeader')

# undo e verifica
ui.command('undo')									# undo method_list
ui.command('undo')									# undo method_list
ui.command('undo')									# undo method_del handleMsg
ui.command('method_list')
ui.assert_success('getLeader', 'handleMsg')

# caso base e verifica
ui.command('method_del getLeader')
ui.assert_success()
ui.command('method_list')
ui.assert_missing_success('getLeader')