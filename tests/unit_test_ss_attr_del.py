import sys
sys.path.insert(0, '../src')
from init import *


# setup per caso base
ui.command('proj_open ringLeader')
ui.command('ss_create ss1')
ui.command('ss_attr_create ss1 attr1 var11:Integer,var12:Integer var11<var12,var11=var12,var11>var12')
ui.command('ss_attr_create ss1 attr2 var21:Integer,var22:Integer var21<var22,var21=var22,var21>var22')

# casi limite
ui.command('ss_attr_del wrongSs wrongAttr')			# spazio degli stati
ui.assert_error('wrongSs', 'doesn\'t exist')
ui.command('ss_attr_del ss1 wrongAttr')				# attributo da eliminare inesistente
ui.assert_error('wrongAttr', 'doesn\'t exist')

# caso base e verifica
ui.command('ss_attr_del ss1 attr1')
ui.assert_success()
ui.command('ss_show ss1')
ui.assert_missing_success('attr1')

# caso base e verifica
ui.command('ss_attr_del ss1 attr2')
ui.assert_success()
ui.command('ss_show ss1')
ui.assert_missing_success('attr1', 'attr2')

# undo e verifica
ui.command('undo')									# undo ss_show ss1
ui.command('undo')									# undo ss_attr_del ss1 attr2
ui.command('ss_show ss1')
ui.assert_success('attr2')

# undo e verifica
ui.command('undo')									# undo ss_show ss1
ui.command('undo')									# undo ss_show ss1
ui.command('undo')									# undo ss_attr_del ss1 attr1
ui.command('ss_show ss1')
ui.assert_success('attr2', 'attr1')

# caso base e verifica
ui.command('ss_attr_del ss1 attr2')
ui.assert_success()
ui.command('ss_show ss1')
ui.assert_missing_success('attr2')