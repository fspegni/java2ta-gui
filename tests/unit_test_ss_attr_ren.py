import sys
sys.path.insert(0, '../src')
from init import *


# setup
ui.command('proj_open ringLeader')
ui.command('ss_create ss1')
ui.command('ss_attr_create ss1 attr1 var11:Integer,var12:Integer var11<var12,var11=var12,var11>var12')
ui.command('ss_attr_create ss1 attr2 var21:Integer,var22:Integer var21<var22,var21=var22,var21>var22')

# casi limite
ui.command('ss_attr_ren wrongSs attr1 attr3')		# spazio degli stati inesistente
ui.assert_error('wrongSs', 'doesn\'t exist')
ui.command('ss_attr_ren ss1 attr1 attr1')			# stesso nome
ui.assert_error()
ui.command('ss_attr_ren ss1 wrongAttr name')		# attributo da rinominare inesistente
ui.assert_error('wrongAttr', 'doesn\'t exist')
ui.command('ss_attr_ren ss1 attr1 attr2')			# nuovo nome gia' associato ad un altro attributo
ui.assert_error('another attribute', 'attr2')

# caso base e verifica
ui.command('ss_attr_ren ss1 attr1 attr3')
ui.assert_success()
ui.command('ss_show ss1')
ui.assert_success('attr3')
ui.assert_missing_success('attr1')
ui.command('ss_attr_ren ss1 attr1 attr4')			# attributo da rinominare inesistente
ui.assert_error('attr1', 'doesn\'t exist')

# caso base e verifica
ui.command('ss_attr_ren ss1 attr3 attr4')
ui.assert_success()
ui.command('ss_show ss1')
ui.assert_success('attr4')
ui.assert_missing_success('attr1', 'attr3')

# undo e verifica
ui.command('undo')									# undo ss_show ss1
ui.command('undo')									# undo ss_attr_ren ss1 attr3 attr4
ui.command('ss_show ss1')
ui.assert_success('attr3')
ui.assert_missing_success('attr1', 'attr4')

# undo e verifica
ui.command('undo')									# undo ss_show ss1
ui.command('undo')									# undo ss_show ss1
ui.command('undo')									# undo ss_attr_ren ss1 attr1 attr3
ui.command('ss_show ss1')
ui.assert_success('attr1')
ui.assert_missing_success('attr3', 'attr4')

# caso base e verifica (variabili e predicati non sono cambiati)
ui.command('ss_attr_ren ss1 attr1 attr3')
ui.assert_success()
ui.command('ss_show ss1')
ui.assert_success('attr3', 'attr2', 'var11', 'var12', 'var21', 'var22', 'Integer', 'var11 < var12', 'var21 < var22')