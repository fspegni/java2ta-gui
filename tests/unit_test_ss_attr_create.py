import sys
sys.path.insert(0, '../src')
from init import *

# setup
ui.command('ss_create ss1')
ui.command('ss_create ss2')

# caso base e verifica
ui.command('ss_attr_create ss1 attr1 var11:Integer,var12:Integer var11<var12,var11=var12,var11>var12')
ui.assert_success()
ui.command('ss_show ss1')
ui.assert_success('attr1', 'var11', 'var12', 'Integer', 'var11 < var12', 'var11 = var12', 'var11 > var12')

# casi limite
ui.command('ss_attr_create wrongSs attr2 var1:Integer,var2:Integer var1=var2,var1!=var2')	# spazio degli stati inesistente
ui.assert_error('wrongSs', 'doesn\'t exist')
ui.command('ss_attr_create ss1 attr1 var1:Integer,var2:Integer var1=var2,var1!=var2')		# attributo gia' esistente
ui.assert_error('another attribute', 'attr1')
ui.command('ss_attr_create ss1 attr2 var1:Integer,var2:Integer wrongVar=var2,var1!=var2')	# variabile in un predicato non definita
ui.assert_error('wrongVar')
ui.command('ss_attr_create ss1 attr2 var1:Integer,var2:Integer var1=var2,var1!var2')		# operatore non riconosciuto
ui.assert_error('operator', '!', 'not supported')
ui.command('ss_attr_create ss1 attr2 var1:wrongDom,var2:Integer var1=var2,var1!=var2')		# dominio non definito
ui.assert_error('wrongDom')
ui.command('ss_attr_create ss1 attr2 var1:Integer,var2:Integer 1=1,var1!=var2')				# predicato su costanti
ui.assert_error('at least one variable')
ui.command('ss_attr_create ss1 attr2 var1:Integer,var1:Boolean,var2:Integer var1=var2,var1!=var2')	# variabile dichiarata due volte
ui.assert_error('var1', 'declared', 'once')
ui.command('ss_attr_create ss1 attr2 var1:Integer,var2:Integer var1=var1,var1!=var1')		# predicato binario su una variabile
ui.assert_error('var1 = var1', 'same variable')

# caso limite: formato variabili sbagliato
ui.command('ss_attr_create ss1 attr2 var1:Integer:Integer,var2:Integer var1=var2,var1!=var2')
ui.assert_error('variable', 'format')
ui.command('ss_attr_create ss1 attr2 var1,var2:Integer var1=var2,var1!=var2')
ui.assert_error('variable', 'format')
ui.command('ss_attr_create ss1 attr2 var1:,var2:Integer var1=var2,var1!=var2')
ui.assert_error('variable', 'format')
ui.command('ss_attr_create ss1 attr2 :var1,var2:Integer var1=var2,var1!=var2')
ui.assert_error('variable', 'format')

# caso limite: formato predicati sbagliato
ui.command('ss_attr_create ss1 attr2 var1:Integer,var2:Integer var1?var2,var1!=var2')
ui.assert_error('predicate', 'format')
ui.command('ss_attr_create ss1 attr2 var1:Integer,var2:Integer var1*var2,var1!=var2')
ui.assert_error('predicate', 'format')
ui.command('ss_attr_create ss1 attr2 var1:Integer,var2:Integer var1,var1!=var2')
ui.assert_error('predicate', 'format')
ui.command('ss_attr_create ss1 attr2 var1:Integer,var2:Integer var1=var2=var1,var1!=var2')
ui.assert_error('predicate', 'format')

# caso base e verifica
ui.command('ss_attr_create ss2 attr2 var21:Integer,var22:Integer var21<var22,var21=var22,var21>var22')
ui.assert_success()
ui.command('ss_show ss2')
ui.assert_success('attr2', 'var21', 'var22', 'Integer', 'var21 < var22', 'var21 = var22', 'var21 > var22')

# undo e verifica
ui.command('undo')						# undo ss_show ss2
ui.command('undo')						# undo ss_attr_create ss2 attr2 ...
ui.command('ss_list -v')
ui.assert_missing_success('attr2', 'var21', 'var22', 'var21 < var22', 'var21 = var22', 'var21 > var22')

# undo e verifica
ui.command('undo')						# undo ss_list -v
ui.command('undo')						# undo ss_show ss1
ui.command('undo')						# undo ss_attr_create ss1 attr1 ...
ui.command('ss_list -v')
ui.assert_missing_success('attr1', 'var11', 'var12', 'var11 < var12', 'var11 = var12', 'var11 > var12')

# caso base e verifica
ui.command('ss_attr_create ss1 attr1 var31:Integer,var32:Integer var31<var32,var31=var32,var31>var32')
ui.assert_success()
ui.command('ss_show ss1')
ui.assert_success('attr1', 'var31', 'var32', 'Integer', 'var31 < var32', 'var31 = var32', 'var31 > var32')
