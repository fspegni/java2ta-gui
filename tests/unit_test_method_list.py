import sys
sys.path.insert(0, '../src')
from init import *


# casi limite
ui.command('method_list filename')						# un solo argomento (ce ne possono essere 0 o 2)
ui.assert_error('number', 'arguments')
ui.command('method_list filename classname name')		# troppi argomenti
ui.assert_error('number', 'arguments')
ui.command('method_list')								# nessun metodo ancora definito
ui.assert_error('defined', 'any', 'method')
ui.command('method_list RingLeader.java RingLeader')	# nessun progetto aperto
ui.assert_error('open a project first')
ui.command('proj_open test1')							# SETUP
ui.command('method_list WrongFile.java SomeClass')		# file sbagliato
ui.assert_error('file', 'doesn\'t exist')
ui.command('method_list file1.java WrongClass')			# classe sbagliata
ui.assert_error('class', 'doesn\'t exist')
ui.command('method_list file1.java class1')				# classe vuota
ui.assert_error('empty')

# setup per caso base
ui.command('proj_open ringLeader')
ui.command('method_add method1 RingLeader.java RingLeader handleMsg')

# casi base e verifica
ui.command('method_list RingLeader.java RingLeader')	# metodi in una classe
ui.assert_success('handleMsg')
ui.command('method_list')								# metodi definiti
ui.assert_success('method1')
ui.command('method_list -v')							# metodi definiti (verbose)
ui.assert_success('method1', 'ringLeader', 'RingLeader.java', 'handleMsg')