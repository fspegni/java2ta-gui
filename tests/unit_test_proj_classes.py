import sys
sys.path.insert(0, '../src')
from init import *


# casi limite e setup per casi base
ui.command('proj_classes')							# nessun progetto aperto
ui.assert_error('open a project first')
ui.command('proj_open emptyProject')				# progetto vuoto
ui.command('proj_classes')
ui.assert_error('emptyProject', 'is empty')

# setup per caso base
ui.command('proj_open ringLeader')

# casi base e verifica
ui.command('proj_classes')							# base
ui.assert_success('RingLeader.java => RingLeader')
ui.command('proj_classes -v')						# verbose
ui.assert_success('handleMsg')
ui.command('proj_open test1')						# apertura nuovo progetto
ui.command('proj_classes')
ui.assert_success('file1.java => class1')
ui.command('proj_close')							# chiusura progetto
ui.command('proj_classes')
ui.assert_error('open a project first')
ui.command('undo')									# chiusura progetto undo, undo proj_classes
ui.command('undo')										# undo proj_classes
ui.command('undo')										# undo proj_open test1
ui.command('proj_classes')								# progetto aperto ringLeader
ui.assert_success('RingLeader.java => RingLeader')
ui.command('undo')										# undo proj_classes
ui.command('undo')										# undo proj_classes -v
ui.command('undo')										# undo proj_classes
ui.command('undo')										# undo proj_open ringLeader
ui.command('proj_classes')								# progetto aperto emptyProject
ui.assert_error('emptyProject', 'is empty')
ui.command('undo')										# undo proj_open emptyProject
ui.command('proj_classes')								# nessun progetto aperto
ui.assert_error('open a project first')