import sys
sys.path.insert(0, '../src')
from init import *


# casi limite
ui.command('method_add handleMsg RingLeader.java RingLeader')				# nessun progetto aperto
ui.assert_error('open a project first')

# setup per caso base e altri casi limite, caso base e verifica
ui.command('proj_open ringLeader')
ui.command('method_add handleMsg RingLeader.java RingLeader')
ui.assert_success('handleMsg')
ui.command('method_list')
ui.assert_success('handleMsg')

# casi limite
ui.command('method_add handleMsg AnotherFile.java AnotherClass')			# nome associato ad un metodo gia' importato
ui.assert_error('associated', 'another', 'method')
ui.command('method_add method WrongFile.java WrongClass')					# file o classe inesistente
ui.assert_error('current project', 'file', 'class', 'exist')
ui.command('method_add wrongMethod RingLeader.java RingLeader')				# metodo inesistente
ui.assert_error('class', 'method', 'exist')
ui.command('method_add anotherName RingLeader.java RingLeader handleMsg')	# metodo gia' importato ma con diverso nome
ui.assert_error('already', 'imported', 'handleMsg')

# caso base e verifica
ui.command('proj_open test2')
ui.command('method_add method1 file2.java class2')
ui.assert_success('method1')
ui.command('method_list')
ui.assert_success('handleMsg', 'method1')

# undo e verifica
ui.command('undo')															# undo method_list
ui.command('undo')															# undo method_add method1 file2 class2
ui.command('method_list')
ui.assert_success('handleMsg')
ui.assert_missing_success('method1')

# undo e verifica
ui.command('undo')															# undo method_list
ui.command('undo')															# undo proj_open test2
ui.command('undo')															# undo method_list
ui.command('undo')															# undo method_add handleMsg RingLeader.java RingLeader
ui.command('method_list')
ui.assert_error('defined', 'any', 'method')

# caso base e verifica
ui.command('method_add getLeader RingLeader.java RingLeader')
ui.assert_success('getLeader')
ui.command('method_list')
ui.assert_success('getLeader')