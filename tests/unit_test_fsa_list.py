import sys
sys.path.insert(0, '../src')
from init import *


# casi limite
ui.command('fsa_list')								# nessuno fsa ancora definito
ui.assert_error('defined', 'any', 'FSA')

# setup per caso base
ui.command('proj_open ringLeader')
ui.command('method_add test1 RingLeader.java RingLeader')
ui.command('ss_create ss1')
ui.command('ss_attr_create ss1 attr1 a:Integer,b:Integer a=b,a!=b')
ui.command('fsa_make test1 ss1 fsa1')
ui.assert_success()

# eliminazione manuale dei file creati (per non lasciare tracce dell'esecuzione del test (che abbia successo o meno)
############################# TODO

# casi base e verifica
ui.command('fsa_list')								# fsa definiti
ui.assert_success('fsa1')
ui.command('fsa_list -v')							# fsa definiti (verbose)
ui.assert_success('fsa1')
ui.assert_success('test1', 'ringLeader', 'RingLeader.java')
ui.assert_success('ss1', 'attr1', 'Integer', 'a = b', 'a != b')
