#!/bin/bash

pushd tests

for file in *.py; do
	if python $file; then
		echo OK $file
	else
		echo test $file failed
		exit 0
	fi
done
echo success!

popd
