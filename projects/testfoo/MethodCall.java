/**
 * Created by Francesco Spegni
 */
public class MethodCall {

//    String election = "election";
//    String leader = "leader";

    public static void main(String[] args) {
        foo(5);
    }

    public static int foo(String tag, int number) {
    
//        int j = MethodCall.getMessageInt(); // get the number

        if (tag.equals("election")) {
            if (j > number) {
                System.out.println("foo");
            }
            else if (j == number) // I won! 
            {
                System.out.println("fie");      
            }
            else if (j < number) 
            {   
                System.out.println("fix!");
            }
        } else if (tag.equals("leader")) {
            System.out.println("fuz");
        }
    }

    public static int getMessageInt(int i) {
        return i + 1;
    }

}
