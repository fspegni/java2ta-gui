public class TestClass {

    public int x = 0;
    public int y = 1;

	// a = b, a != b
	public void testMinimal(int a, int b){
		a++;
		b++;
	}

	// a = 0, a != 0
	public void testIfConst(int a){
		if(a == 0){
			a++;
		}
	}

	// a = "mickey", a = "scrooge", a != "mickey" and a != "scrooge"
	public void testIfConstStrings(String a){
		if(a.equals("mickey")){
			a = "goofy";
		}
	}



	// a = b, a != b
	public void testIfVar(int a, int b){
		if(a == b){
			b++;
		}
	}

	// a = 0, a != 0
	public void testIfElseConst(int a){
		if(a == 0){
			a++;
		} else{
			a--;
		}
	}

	// a = b, a != b
	public void testIfElseVar(int a, int b){
		if(a == b){
			b++;
		} else{
			b--;
		}
	}

	// a < 0, a < 3, a > 2
	public void testWhileConst(int a){
		int i = 5;
		while(i > 0){
			i--;
			a++;
		}
	}

	// a = b, a != b
	public void testWhileVar(int a, int b){
		int i = 10;
		while(i > 0){
			i--;
			if(i % 3 == 0)
				a++;
			else
				b++;
		}
	}

    public void testCallee(int c, int d) {
        this.x = c;
        this.y = d;
    }

	// a < b, a = b, a > b 
    // x < y, x = y, x > y
	public void testMethodCall(int a, int b){
        this.x = b;
        this.testCallee(a, b);
	}


	// a < 0, a < 3, a > 2
	public void testForConst(int a){
		for(int i = 0; i < 5; i++){
			i--;
			a++;
		}
	}

	// a = b, a != b
	public void testForVar(int a, int b){
		for(int i = 0; i < 10; i++){
			i--;
			if(i % 3 == 0)
				a++;
			else
				b++;
		}
	}

	// a = b, a != b
	public void testBreak(int a, int b){
		for(int i = 0; i < 10; i++){
			if(i % 3 == 0)
				a++;
			else
				b++;
			if(a == b)
				break;
		}
	}

}
