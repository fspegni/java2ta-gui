public class TestThread extends Thread {
    int x;
    int id;

    public TestThread(int id) {
        this.id = id;
    }
    
    public void run() {

        int param = 5;
        while (true) {

//            System.out.println(this.id + ": x = " + this.x);
            if (x > param) {
                //System.out.println(this.id + ": x (" + this.x + ") > " + param);
                System.out.println("high");
                x--;
            } else {
                x++;
            }


            try {
                Thread.sleep(x * 200);
            }
            catch (Exception e) {
                //System.out.println(this.id + ": esci");
                System.out.println("exit");
                break;
            }
        }       
    }

    public static void main(String[] args) {
        for (int i=0; i<5; i++) {
            System.out.println("Create thread: " + i);
            TestThread t = new TestThread(i);
            t.start();
        }   
    }
}
