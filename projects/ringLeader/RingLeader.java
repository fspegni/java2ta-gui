public class RingLeader extends Process implements Election {
    int number;    
    int leaderId = -1;
    int next;   
    boolean awake = false;
    public RingLeader(Linker initComm, int number) {
        super(initComm);
        this.number = number;
        next = (myId + 1) % N;
    }
    public synchronized int getLeader(){
	while (leaderId == -1) myWait();
	return leaderId;
    }
    public void test(int a, int b){
	if(a == b){
		b++;
	} else{
		b--;
	}
    }
    public void test1(int a, int b){
	a++;
	b++;
    }
    public void test2(int a){
	a = 0;
    }
    public synchronized void handleMsg(Msg m, int src, String tag) {

	int i = 0;
        int j = 3; //m.getMessageInt(); // get the number
        if (tag.equals("election")) {
            if (j > number)
                i = 1; //sendMsg(next, "election", j); // forward the message
            else if (j == number) // I won!
                i = 2; //sendMsg(next, "leader", myId);
            else if ((j < number) && !awake) i = 5; //startElection();
        } else if (tag.equals("leader")) {
            leaderId = j;
	    i = 3; //notify();
            if (j != myId) i = 4; //sendMsg(next, "leader", j);
        }
    }
    public synchronized void startElection() {
        awake = true;
        sendMsg(next, "election", number);
    }
}
