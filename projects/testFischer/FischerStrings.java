
import static java.lang.Thread.sleep;
import java.util.Scanner;


import java.util.logging.Level;
import java.util.logging.Logger;

public class FischerStrings extends Thread {
    public static final int DELTA = 50;
    public static String x = null;
    public static int y=0;

    final String id;
    long c,c1; 
    Logger logger;

    public FischerStrings (String a)
    {
        this.id = a;
        this.logger = Logger.getLogger(FischerStrings.class.getName());
    }


    public void enterCS() throws InterruptedException {

        do {
            while (x != null) {
                //System.out.println("wait"); // no-op
                this.sleep(DELTA);
            }
            x = this.id;
            this.sleep(DELTA);
        } while (! this.id.equals(x));    //postcondition

        // begin critical section
        System.out.println(this.id + " enters CS ...");
        y++;
        y--;
        System.out.println(this.id + " exits CS ...");
        // end critical section
        x = null;   
   }

    public void run() {
        try {
            this.enterCS();
        } catch (InterruptedException e) {
            System.err.println("Interrupted exception ...");
        }
    }
 

     /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int nthreads = 50;
        if (args.length > 0) {
            nthreads = Integer.parseInt(args[0]);
        }
        
        System.out.println("Assume " + nthreads + " threads ...");
        for (int co = 1; co <= nthreads; co++){
            FischerStrings p = new FischerStrings("thread_" + co);
            p.start();
        }
    }
}

