
import static java.lang.Thread.sleep;
import java.util.Scanner;


import java.util.logging.Level;
import java.util.logging.Logger;

public class FischerBuggy extends Thread {
    public static final int DELTA = 50;
    public static int x,y=0;

    final int i;
    long c,c1; 
    Logger logger;

    public FischerBuggy (int a)
    {
        this.i = a;
        this.logger = Logger.getLogger(FischerBuggy.class.getName());
    }

    public void run() {

        try {
            do {
                while (FischerBuggy.x != 0) {          //from here
                    System.out.println("busy-waiting"); // no-op
                }                  // await: FischerBuggy.x = 0    to here is the precondition
                FischerBuggy.x = this.i;
                this.sleep(DELTA);
            } while (FischerBuggy.x != this.i);    //postcondition

            // begin critical section
            y++;
            //y--; // commenting this should introduce a bug
            // end critical section
            FischerBuggy.x = 0;   
        } catch (InterruptedException e) {
            // ...
        }
   }
 

     /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int nthreads = 50;
        if (args.length > 0) {
            nthreads = Integer.parseInt(args[0]);
        }
        
        System.out.println("Assume " + nthreads + " threads ...");
        for (int co = 1; co <= nthreads; co++){
            FischerBuggy p = new FischerBuggy(co);
            p.start();
        }
    }
}

