
import static java.lang.Thread.sleep;
import java.util.Scanner;


import java.util.logging.Level;
import java.util.logging.Logger;

public class Fischer extends Thread {
    public static final int DELTA = 50;
    public static int x,y=0;

    final int i;
    long c,c1; 
    Logger logger;

    public Fischer (int a)
    {
        this.i = a;
        this.logger = Logger.getLogger(Fischer.class.getName());
    }

    public void run() {

        try {
            do {
                while (Fischer.x != 0) {          //from here
/*                    try {
                        // do nothing
                        this.sleep(1);
                    } catch (InterruptedException ex) {
                        // ...
                    } */
                    System.out.println("wait"); // no-op
                }                  // await: Fischer.x = 0    to here is the precondition
                Fischer.x = this.i;
                this.sleep(DELTA);
            } while (Fischer.x != this.i);    //postcondition

            // begin critical section
            y++;
            y--;
            // end critical section
            Fischer.x = 0;   
        } catch (InterruptedException e) {
            // ...
        }
   }
 

     /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int nthreads = 50;
        if (args.length > 0) {
            nthreads = Integer.parseInt(args[0]);
        }
        
        System.out.println("Assume " + nthreads + " threads ...");
        for (int co = 1; co <= nthreads; co++){
            Fischer p = new Fischer(co);
            p.start();
        }
    }
}

