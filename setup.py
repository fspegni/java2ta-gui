try:
	from setuptools import setup, find_packages
except ImportError:
	raise ImportError("Install setup tools")

config = {
	'description': 'JaMC : A model checker for Java code',
	'author': 'Francesco Spegni',
	'url': 'https://bitbucket.org/fspegni/java2ta-gui.git',
	'download_url': 'TODO',
	'author_email': 'f.spegni@univpm.it',
	'version': '0.1',
	'install_requires': [
		'argparse>=1.2.1',
		'prompt-toolkit>=1.0.8',
		'GitPython>=2.1.7',
		'Pygments>=2.1.3',
        'deprecated>=1.1.0',
		'java2ta',
	],
	'packages': [ 'jamc',], #find_packages(),
##	'package_dir': {
##		'': '.', #src',
##	},
	'entry_points': {
		# the format of each console_script item is: name=package.module:function_name
		'console_scripts': [ 'jamc=jamc:run' ],
	},
	'name': 'jamc',
	'license': 'MIT',
	'classifiers': [

		# How mature is this project? Common values are
		#   3 - Alpha
		#   4 - Beta
		#   5 - Production/Stable
		'Development Status :: 3 - Alpha',

		# Indicate who your project is intended for
		'Intended Audience :: Science/Research',
		'Topic :: Security',
		'Topic :: Scientific/Engineering',

		# Pick your license as you wish (should match "license" above)
		 'License :: OSI Approved :: MIT License',

		# Specify the Python versions you support here. In particular, ensure
		# that you indicate whether you support Python 2, Python 3 or both.
		'Programming Language :: Python :: 2.7',
	],
	'keywords': 'software model checking formal methods uppaal'
}


setup(**config)

